""" 
-------- EXERCISE 10 - problem 3 ------------
----- FYS-4096 - Computational Physics -----

Calculates energies from textfiles
"""

from numpy import *



def main():
    E0 = loadtxt("S0_E_tot.dat", unpack=True)
    E3 = loadtxt("S3_E_tot.dat", unpack=True)
    print(f'S=0 total energy: {mean(E0):.5f} +/- {std(E0)/sqrt(len(E0)):0.5f}')
    print(f'S=3 total energy: {mean(E3):.5f} +/- {std(E3) / sqrt(len(E3)):0.5f}')

if __name__ == '__main__':
    main()
