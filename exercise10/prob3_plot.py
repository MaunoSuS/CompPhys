""" 
-------- EXERCISE 10 - problem 3 ------------
----- FYS-4096 - Computational Physics -----

Plot electron densities
"""

from matplotlib.pyplot import *
from numpy import *
from mpl_toolkits.mplot3d import Axes3D


def main():
    file = loadtxt("S0_ag_density.dat", unpack=True)
    fig1 = figure()
    ax1 = Axes3D(fig1)

    ax1.plot_trisurf(file[0,:], file[1,:], file[2,:], cmap='inferno')
    ax1.set_title('S=0')
    ax1.set_xlabel('X')
    ax1.set_ylabel('Y')
    ax1.set_zlabel(r'$\rho$')
    
    file = loadtxt("S3_ag_density.dat", unpack=True)
    fig2 = figure()
    ax2 = Axes3D(fig2)

    ax2.plot_trisurf(file[0,:], file[1,:], file[2,:], cmap='inferno')
    ax2.set_title('S=3')
    ax2.set_xlabel('X')
    ax2.set_ylabel('Y')
    ax2.set_zlabel(r'$\rho$')
    show()

if __name__ == '__main__':
    main()
