----------------- EXERCISESET 10 ------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: a) Calculated average total energy of a 2d quantum dot
             Output:
                PIMC total energy: 3.12866 +/- 0.04363
                Variance to energy ratio: 0.09126
             (tau = 0.25, M = 100, k = 3.16681e-6)
             T = 1/(tau*k*M) = 12631 K
          
          b) commenting
             
          c) Output:
             
              for quantum dot gauss 
             PIMC total energy: 2.85005 +/- 0.03451
             Variance to energy ratio: 0.06267
             Temperature: 12631.00723 K
 
             Extrapolated energy at tau = 0: 4.21906 

              for hydrogen molecule gauss 
             PIMC total energy: 1.82075 +/- 0.10222
             Variance to energy ratio: 0.86083
             Temperature: 31577.51807 K
             
             Extrapolated energy at tau = 0: 3.97470 
 
          d) Output with uniform step:
          
              for quantum dot uniform 
             PIMC total energy: 2.99399 +/- 0.03849
             Variance to energy ratio: 0.07423
             Temperature: 12631.00723 K

              for hydrogen molecule uniform 
             PIMC total energy: 4.44774 +/- 0.12448
             Variance to energy ratio: 0.52255
             Temperature: 31577.51807 K

             Convergence is slower and each iteration moves the energy less
             uniform step energy values are higher since some of the values
             contained in mean are not yet converged the total energy is higher
                     
problem2: Calculated periodic free particle density matrix three different waves 
          shown on the exercise sheet with different beta and n values and
          plotted them on a figure
          
problem3: Calculated 2d 6 electron semiconductor quantumdot energies:
            S=0 total energy: 20.44241 +/- 0.01718
            S=3 total energy: 20.44327 +/- 0.02244
          These results are also plotted

problem4: energy of graphene with vmc and dmc.
          scf output: !    total energy              =     -22.75523572 Ry
          vmc calculation would not end for some reason.
          Tried to reinstall qmcpack but didn't work.
          
