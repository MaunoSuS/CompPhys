""" 
-------- EXERCISE 10 - problem 2 ------------
----- FYS-4096 - Computational Physics -----

Calculates periodic free particle density matrix three different waves shown
on the exercise sheet

From the figure this outputs we see that the Gaussian form is a good
approximation at higher temperatures (small beta values)

In order to make this three dimensional we would need to add the third component
to vectors. However showing the results in an illustrative manner would 
require some extra pondering.
"""


from numpy import *
from matplotlib.pyplot import *

def gaussian(ax, R, Rp, lam, b, d):

    ax.contourf(R,Rp,(4*pi*lam*b)**(-d)*exp(-(R-Rp)**2/(4*lam*b)))
    ax.set_title(f'Gaussian, beta = {b}')
    
    return ax
    
def periodic_1(ax, R, Rp, x, L, b, lam, nmax, d):
    
    n = arange(0, nmax)
    E = lam * (2 * pi * n / L) ** 2
    rho = zeros(R.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            rho[i, j] = sum(L**(-d/2)*2*cos(-b*E*(R[i,j]-Rp[i,j])))

    ax.contourf(R, Rp, rho)
    ax.set_title(f'Option 1, n_max = {nmax}, beta = {b}')
    return ax

def periodic_2(ax, R, Rp, x, L, b, lam, nmax, d):
    
    n = arange(-nmax, nmax)
    rho = zeros(R.shape)
    for i in range(len(x)):
        for j in range(len(x)):
            rho[i, j] = sum((4*pi*lam*b)**(-d)*exp(-(R[i,j]+n*L-Rp[i,j])**2/(4*lam*b)))
    
    ax.contourf(R, Rp, rho)
    ax.set_title(f'Option 2, n_max = {nmax}, beta = {b}')
    
    return ax
   
def main():
    L = 2
    nmax = 1000
    lam = 0.5
    d = 1
    b = 0.001 #1/(1.38e-23*4)
    x = linspace(-L / 2, L / 2)
    R, Rp = meshgrid(x, x)
    
    fig = figure()
    
    ax = [0,0,0,0,0,0,0,0,0,0,0,0]
    
    ax[0] = gaussian(fig.add_subplot(431), R, Rp, lam, 1, d)
    ax[1] = periodic_1(fig.add_subplot(432), R, Rp, x, L, 1, lam, 500, d)
    ax[2] = periodic_2(fig.add_subplot(433), R, Rp, x, L, 1, lam, 500, d)
    ax[3] = gaussian(fig.add_subplot(434), R, Rp, lam, 0.1, d)
    ax[4] = periodic_1(fig.add_subplot(435), R, Rp, x, L, 0.1, lam, 10, d)
    ax[5] = periodic_2(fig.add_subplot(436), R, Rp, x, L, 0.1, lam, 10, d)
    ax[6] = gaussian(fig.add_subplot(437), R, Rp, lam, 0.01, d)
    ax[7] = periodic_1(fig.add_subplot(438), R, Rp, x, L, 0.01, lam, 100, d)
    ax[8] = periodic_2(fig.add_subplot(439), R, Rp, x, L, 0.01, lam, 100, d)
    ax[9] = gaussian(fig.add_subplot(4,3,10), R, Rp, lam, 0.001, d)
    ax[10] = periodic_1(fig.add_subplot(4,3,11), R, Rp, x, L, 0.001, lam, 1000, d)
    ax[11] = periodic_2(fig.add_subplot(4,3,12), R, Rp, x, L, 0.001, lam, 1000, d)
    
    for axes in ax:
        axes.label_outer()
    show()


if __name__ == '__main__':
    main()
