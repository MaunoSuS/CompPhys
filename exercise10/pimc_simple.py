#! /usr/bin/env python3

from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf

class Walker:
    """
    Objects that are created as this class store all the necessary information
    for solving the system with monte carlo method. Also some variables for
    optimization
    """
    def __init__(self,*args,**kwargs):
        # number of electrons
        self.Ne = kwargs['Ne']
        # position of electrons
        self.Re = kwargs['Re']
        # spins as 0 and 1
        self.spins = kwargs['spins']
        # number of nuclei
        self.Nn = kwargs['Nn']
        # position of nuclei
        self.Rn = kwargs['Rn']
        # charge of nuclei
        self.Zn = kwargs['Zn']
        # tau = beta/M
        self.tau = kwargs['tau']
        # dimensions
        self.sys_dim = kwargs['dim']
        # type as nucleus or qdot
        self.o_type = kwargs['o_type']

    def w_copy(self):
        # returns values
        return Walker(Ne=self.Ne,
                      Re=self.Re.copy(),
                      spins=self.spins.copy(),
                      Nn=self.Nn,
                      Rn=self.Rn.copy(),
                      Zn=self.Zn,
                      tau=self.tau,
                      dim=self.sys_dim,
                      o_type=self.o_type)
    

def kinetic_action(r1,r2,tau,lambda1):
    """
    potential energy for walker action
    """
    return sum((r1-r2)**2)/lambda1/tau/4

def potential_action(Walkers,time_slice1,time_slice2,tau):
    """
    potential energy approximation for walker action
    """
    return 0.5*tau*(potential(Walkers[time_slice1]) \
                    +potential(Walkers[time_slice2]))

def pimc(Nblocks,Niters,Walkers,gauss):
    """
    uses path integral monte carlo to calculate energies and wave functions
    
    :param: Nblocks    number of blocks of iterations
    :param: Niters     number of iterations in block
    :param: Walkers    initial walkers that setup the system
    
    :return: Walkers   changed walkers
    :return: Eb        Energies for each block and the corresponding 
                       wavefunction
    :return: Accept    acceptance ratio for mmc iteration results
    """
    
    # number of points in "imaginary" path
    M = len(Walkers)
    Ne = Walkers[0].Ne*1
    sys_dim = 1*Walkers[0].sys_dim
    # imaginary time-step
    tau = 1.0*Walkers[0].tau
    # constant for kinetic terms
    lambda1 = 0.5
    Eb = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))
    # variance for gauss distributed random movement
    sigma2 = lambda1*tau
    # std ^
    sigma = sqrt(sigma2)

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            # random time in range [0, M]
            time_slice0 = int(random.rand()*M)
            # two consecutive timesteps (or roll over to start of [0,M])
            time_slice1 = (time_slice0+1)%M
            time_slice2 = (time_slice1+1)%M
            # random electron to move
            ptcl_index = int(random.rand()*Ne)

            # reference position and positions to move
            r0 = Walkers[time_slice0].Re[ptcl_index]
            r1 = 1.0*Walkers[time_slice1].Re[ptcl_index]
            r2 = Walkers[time_slice2].Re[ptcl_index]
            
            # action before moving
            KineticActionOld = kinetic_action(r0,r1,tau,lambda1) +\
                kinetic_action(r1,r2,tau,lambda1)
            PotentialActionOld = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            # bisection sampling
            # r02_ave = (r0+r2)/2
            #log_S_Rp_R = -sum((r1-r02_ave)**2)/2/sigma2             
            #Rp = r02_ave + random.randn(sys_dim)*sigma
            #log_S_R_Rp = -sum((Rp - r02_ave)**2)/2/sigma2
            
            if gauss:
                Rp, log_S_Rp_R, log_S_R_Rp = bisection_gauss_move(r0, r1, r2, sys_dim, sigma, sigma2)
            else:
                Rp, log_S_Rp_R, log_S_R_Rp = uniform_move(r1,sys_dim,sigma)
            

            
            # save new position and calculate new action function values
            Walkers[time_slice1].Re[ptcl_index] = 1.0*Rp
            KineticActionNew = kinetic_action(r0,Rp,tau,lambda1) +\
                kinetic_action(Rp,r2,tau,lambda1)
            PotentialActionNew = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            # action difference
            deltaK = KineticActionNew-KineticActionOld
            deltaU = PotentialActionNew-PotentialActionOld
            #print('delta K', deltaK)
            #print('delta logS', log_S_R_Rp-log_S_Rp_R)
            #print('exp(dS-dK)', exp(log_S_Rp_R-log_S_R_Rp-deltaK))
            #print('deltaU', deltaU)
            
            # improvement ratio and acceptance
            q_R_Rp = exp(log_S_Rp_R-log_S_R_Rp-deltaK-deltaU)
            A_RtoRp = min(1.0,q_R_Rp)
            
            # check acceptance against random number
            if (A_RtoRp > random.rand()):
                Accept[i] += 1.0
            else:
                # if not accepted return to original pos
                Walkers[time_slice1].Re[ptcl_index]=1.0*r1
            # acceptance count for a block
            AccCount[i] += 1
            
            # calculate energy every interval
            if j % obs_interval == 0:
                E_kin, E_pot = Energy(Walkers)
                #print(E_kin,E_pot)
                Eb[i] += E_kin + E_pot
                EbCount += 1
            #exit()
        
        # final energies and acceptance
        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept


def Energy(Walkers):
    """
    Energy of electrons as average of walkers
    """
    M = len(Walkers)
    d = 1.0*Walkers[0].sys_dim
    tau = Walkers[0].tau
    lambda1 = 0.5
    U = 0.0
    K = 0.0
    for i in range(M):
        U += potential(Walkers[i])
        for j in range(Walkers[i].Ne):
            if (i<M-1):
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[i+1].Re[j])**2)/4/lambda1/tau**2
            else:
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[0].Re[j])**2)/4/lambda1/tau**2    
    return K/M,U/M
        
    

def potential(Walker):
    """
    Energy of molecule (external and between)
    """
    V = 0.0
    r_cut = 1.0e-12
    # e-e
    for i in range(Walker.Ne-1):
        for j in range(i+1,Walker.Ne):
            r = sqrt(sum((Walker.Re[i]-Walker.Re[j])**2))
            V += 1.0/max(r_cut,r)
    
    # external potential depends on type of object
    if Walker.o_type == "qdot":
        Vext = external_potential(Walker)
    elif Walker.o_type == "nucleus":
        Vext = potential_nucleus(Walker)
    
    return V+Vext


def external_potential(Walker):
    """
    external potential of quantum dot (parabolic)
    """
    V = 0.0
    for i in range(Walker.Ne):
        V += 0.5*sum(Walker.Re[i]**2)
        
    return V

def potential_nucleus(Walker):
    """
    extrernal potential for nuclei
    """
    V = 0.0
    r_cut = 1.0e-12

    # Ion - E
    for i in range(Walker.Nn):
        for j in range(Walker.Ne):
            r = sum((Walker.Re[j] - Walker.Nn) ** 2)
            V -= Walker.Zn[i] * erf(r / sqrt(2 * 0.1)) / r
    
    # potential between nuclei
    for i in range(Walker.Nn - 1):
        for j in range(i + 1, Walker.Nn):
            r = np.sqrt(sum((Walker.Rn[i] - Walker.Rn[j]) ** 2))
            V += 1.0 / max(r_cut, r)
    return V


def uniform_move(r1, sys_dim, sigma):

    log_S_Rp_R = 0
    Rp = r1 + (random.rand(sys_dim)-1/2)*2*sigma
    log_S_R_Rp = 0

    return Rp, log_S_Rp_R, log_S_R_Rp

def bisection_gauss_move(r0, r1, r2, sys_dim, sigma, sigma2):
    # bisection sampling (as in dissertation)
    r02_ave = (r0 + r2) / 2
    log_S_Rp_R = -sum((r1 - r02_ave) ** 2) / 2 / sigma2  # Gauss sampling probability
    # Gauss distributed random move of r1 in regards to the reference
    # "bisection" of r0 and r2:
    Rp = r02_ave + random.randn(sys_dim) * sigma
    log_S_R_Rp = -sum((Rp - r02_ave) ** 2) / 2 / sigma2  # Gauss sampling probability

    return Rp, log_S_Rp_R, log_S_R_Rp


def main():
    
    figs = [figure(), figure()]
    
    axes = [figs[0].add_subplot(131), figs[0].add_subplot(132), figs[0].add_subplot(133), \
            figs[1].add_subplot(131), figs[1].add_subplot(132), figs[1].add_subplot(133)]
    
    
    final_msg = ""
    
    k = 0
    for name in ["quantum dot", "hydrogen molecule"]:
        figs[k].suptitle(f'PIMC of {name}')
        kk = 0
        for gauss in [True, False]:
            if gauss:
                name_1 = "gauss"
            else:
                name_1 = "uniform"
                
            Walkers=[]

            if k == 1:
                # For H2
                Walkers.append(Walker(Ne=2,
                                      Re=[array([0.5,0,0]),array([-0.5,0,0])],
                                      spins=[0,1],
                                      Nn=2,
                                      Rn=[array([-0.7,0,0]),array([0.7,0,0])],
                                      Zn=[1.0,1.0],
                                      tau = 0.1,
                                      dim=3,
                                      o_type="nucleus"))
            else :

                # For 2D quantum dot
                Walkers.append(Walker(Ne=2,
                                      Re=[array([0.5,0]),array([-0.5,0])],
                                      spins=[0,1],
                                      Nn=2, # not used
                                      Rn=[array([-0.7,0]),array([0.7,0])], # not used
                                      Zn=[1.0,1.0], # not used
                                      tau = 0.25,
                                      dim=2,
                                      o_type="qdot"))
            
            
            M=100
            for i in range(M-1):
                 Walkers.append(Walkers[i].w_copy())
            Nblocks = 200
            Niters = 100
            
            Walkers, Eb, Acc = pimc(Nblocks,Niters,Walkers, gauss)
            
            
            axes[3*k+kk].plot(Eb)
            conv_cut=50
            axes[3*k+kk].plot([conv_cut,conv_cut],axes[3*k+kk].get_ylim(),'k--')
            Eb = Eb[conv_cut:]
            axes[3*k+kk].axhline(mean(Eb), c='r')
            
            axes[3*k+kk].set_xlabel('blocks')
            axes[3*k+kk].set_ylabel('E')
            axes[3*k+kk].set_title(f'{name_1}')
            
            print(f"\n\n For {name} {name_1} \n\n")
            
            print('PIMC total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
            print('Variance to energy ratio: {0:.5f}'.format(abs(var(Eb)/mean(Eb)))) 
            print(f"Temperature: {1/(Walkers[0].tau*M*3.16681e-6):.5f} K")
            print("\n\n")
            
            final_msg += f"\n for {name} {name_1} \nPIMC total energy: {mean(Eb):.5f} +/-"\
                       + f" {std(Eb)/sqrt(len(Eb)):0.5f}\nVariance to energy ratio:"\
                       + f" {abs(var(Eb)/mean(Eb)):.5f}\n" \
                       + f"Temperature: {1/(Walkers[0].tau*M*3.16681e-6):.5f} K\n"
            
            kk += 1
        # end for 
        
        
        E = []
        i = 0
        steps = [0.05, 0.15, 0.25, 0.35]
        for step in steps:
            Walkers=[]

            if k == 1:
                # For H2
                Walkers.append(Walker(Ne=2,
                                      Re=[array([0.5,0,0]),array([-0.5,0,0])],
                                      spins=[0,1],
                                      Nn=2,
                                      Rn=[array([-0.7,0,0]),array([0.7,0,0])],
                                      Zn=[1.0,1.0],
                                      tau = step,
                                      dim=3,
                                      o_type="nucleus"))
            else :

                # For 2D quantum dot
                Walkers.append(Walker(Ne=2,
                                      Re=[array([0.5,0]),array([-0.5,0])],
                                      spins=[0,1],
                                      Nn=2, # not used
                                      Rn=[array([-0.7,0]),array([0.7,0])], # not used
                                      Zn=[1.0,1.0], # not used
                                      tau = step,
                                      dim=2,
                                      o_type="qdot"))
            
            
            M=100
            for i in range(M-1):
                 Walkers.append(Walkers[i].w_copy())
            Nblocks = 200
            Niters = 100
            
            Walkers, Eb, Acc = pimc(Nblocks,Niters,Walkers, True)
            
            E.append(mean(Eb))
            i += 1
        # end loop
        
        
        z = polyfit(steps,E, 1)
        p = poly1d(z)
        xx = linspace(0, 1)
        axes[3*k+2].plot(steps, E, "x")
        axes[3*k+2].plot(xx, p(xx), "-")
        axes[3*k+2].grid(True)
        axes[3*k+2].set_xlabel('tau')
        axes[3*k+2].set_ylabel('E')
        axes[3*k+2].set_title('time-step extrapolation')
        print(f'Extrapolated energy at tau = 0: {p(0):.5f}')
        
        final_msg += f'\nExtrapolated energy at tau = 0: {p(0):.5f} \n'
        
        figs[k].savefig(f'{name}.jpg')
        
        k += 1
    # end of loop
    print(final_msg)
    show()
    
if __name__=="__main__":
    main()
        
