""" 
-------- Project work 2 - solar system ------------
----- FYS-4096 - Computational Physics -----


"""
import numpy as np
import matplotlib.pyplot as plt
from spline_class import *
import os
import os.path
import deepdish as dd
from mpl_toolkits.mplot3d import Axes3D
import time
from celluloid import Camera

class planetary_object:
    
    def __init__(self, name, mass, radius, aphelion, v_aphelion, inclination, \
                 level, parent_loc, v_parent, c):
        # initial state, and other parameters for the object
        self.name     = name
        self.mass     = float(mass)
        self.r        = float(radius)
        self.a        = float(aphelion)
        self.incl     = float(inclination)*2*np.pi/360
        self.v_a      = [0,\
                         float(v_aphelion)*np.cos(self.incl),\
                         float(v_aphelion)*np.sin(self.incl)]
        self.level    = level
        self.color    = c
        self.loc_p    = parent_loc
        
        # current state
        self.R        = float(aphelion)
        self.v        = [self.v_a[0]+v_parent[0], \
                         self.v_a[1]+v_parent[1], \
                         self.v_a[2]+v_parent[2]]
        self.v_p      = v_parent
        
        
        # orbiting bodies
        self.orbiters = []
        
        # location in Cartesian system. 
        #            x             y = 0 at start     z is the inclination up
        self.loc      = [self.R*np.cos(self.incl)+parent_loc[0], \
                         parent_loc[1], \
                         self.R*np.sin(self.incl)+parent_loc[2]]
    
    def update(self, potential, dt):
        # update velocity with potential and then location
        
        self.v = [self.v[0]+potential[0]*dt,\
                  self.v[1]+potential[1]*dt,\
                  self.v[2]+potential[2]*dt]
        
        self.loc = [self.loc[0] + self.v[0]*dt,\
                    self.loc[1] + self.v[1]*dt,\
                    self.loc[2] + self.v[2]*dt]
        
        self.R = np.sqrt(self.loc[0]**2+self.loc[1]**2+self.loc[2]**2)
        
        # not the most beautiful coding but this was added late
        try:
            self.pot.append(np.linalg.norm(potential))
            self.kin.append(0.5*self.mass*np.linalg.norm(self.v)**2)
        except:
            self.pot      = [np.linalg.norm(potential)]
            self.kin      = [0.5*self.mass*np.linalg.norm(self.v)**2]
    
    def update_loc(self,aphelion,parent_loc):
        # update location if user so states
        
        self.a        = float(aphelion)
        self.R        = float(aphelion)
        self.loc      = [self.R*np.cos(self.incl)+parent_loc[0], \
                         parent_loc[1], \
                         self.R*np.sin(self.incl)+parent_loc[2]]
        self.loc_p    = parent_loc
        
        for orb in self.orbiters:
            orb.update_loc(orb.a,parent_loc)
                        
    def update_v_a(self, v_aphelion, v_parent):
        # update velocity if user so states
        
        self.v_a      = [0,\
                         float(v_aphelion)*np.cos(self.incl),\
                         float(v_aphelion)*np.sin(self.incl)]
        self.v        = [self.v_a[0]+v_parent[0], \
                         self.v_a[1]+v_parent[1], \
                         self.v_a[2]+v_parent[2]]
        self.v_p      = v_parent
        
        # for each orbiter then also
        for orb in self.orbiters:
            orb.update_v_a(orb.v_a,self.v)
        
    
    def update_incl(self):
        # updates location and velocity based on new inclination
        
        v = np.linalg.norm(self.v_a)
        
        self.v_a      = [0,\
                         float(v)*np.cos(self.incl),\
                         float(v)*np.sin(self.incl)]
        
        self.loc      = [self.R*np.cos(self.incl)+self.loc_p[0], \
                         self.loc_p[1], \
                         self.R*np.sin(self.incl)+self.loc_p[2]]
    
    
    
    def print_names(self):
        # prints the names of the object and those that orbit it
        
        msg = ""
        
        for i in range(self.level):
            msg = msg + "  "
        
        msg = msg + self.name
        print(msg)
        
        for orbiter in self.orbiters:
            orbiter.print_names()
            
    def print_names_under(self):
        # prints only orbiters
        
        for orbiter in self.orbiters:
            print(orbiter.name)
        
    def print_attr(self):
        #prints attributes of object
        
        print(f"- name                          : {self.name}")
        print(f"- mass                          : {self.mass}")
        print(f"- radius                        : {self.r}")
        print(f"- aphelion/apogee               : {self.a}")
        print(f"- velocity (at aphelion/apogee) : {self.v_a}")
        print(f"- inclination                   : {self.incl}")
        print(f"- color                         : {self.color}\n")
    
    def sort_orbiters(self):
        # sorts the orbiters according to distance
        
        temp_list = []
        for orb in self.orbiters:
            temp_list.append(orb.R)
        order = np.argsort(temp_list)
        temp_list = []
        i = 0
        for orb in self.orbiters:
            temp_list.append(self.orbiters[order[i]])
            orb.sort_orbiters()
            i = i+1
        self.orbiters = temp_list
        
        
    

def write_h5_data(filename, system):
    """
    Writes to h5 file
    :param: filename    filename of hdf5 file
    :param: system      system to save
    """
    
    filename = "data/" + filename + ".h5"
    dd.io.save(filename,dict(system=system))

def read_h5_file(filename):
    """
    Reads h5 file
    :param:  filename   filename of hdf5 file
    :return:            system loaded
    """
    filename = "data/" + filename + ".h5"
    system = dd.io.load(filename)
    return system['system']

def pre_simulation(system):
    """
    Text based UI for creating and editing a system and then pushes to 
    simulation
    
    :param: system    system of interest
    """
    value = "help"
    orb = []
    
    system.sort_orbiters()
    
    while not(value == "exit"):
        
        curr_system = system
        
        for orb_num in orb:
            curr_system = curr_system.orbiters[orb_num]
    
        print(f"System under {curr_system.name}:")
        curr_system.print_names()
        print("\n")
        print(f"Levels above: {curr_system.level}\n")
        
        if value == "help":
            
            print( "Commands to alter the solar system or to run simulation:\n")
            print(f"add       : add an orbiter for {curr_system.name}")
            print(f"remove    : remove an orbiter from {curr_system.name}")
            print( "alter     : alter an attribute of current object")
            print( "up        : go up a level")
            print( "down      : go down a level")
            print( "save      : save the system into a hdf5 file")
            print( "help      : list commands")
            print( "run       : runs the simulation with given system")
            print( "exit      : exit the program\n")
        
        value = input("CMD > ")
        
        # value defines action that is specified in the help command
        if value == "add":
            
            curr_system.orbiters.append(create_orbiter(curr_system))
        
        elif value == "remove":
            if len(curr_system.orbiters) == 0:
                print("No orbitors under!")
            
            else:
                print("Which orbiter?")
                curr_system.print_names_under()
                orb_name = input("    > ")
                
                b_found = False
                i = 0
                for orbiter in curr_system.orbiters:
                    
                    if orbiter.name == orb_name:
                        curr_system.orbiters.pop(i)
                        b_found = True
                        break
                    
                    i = i+1
                
                if not b_found:
                    print("No such orbitor")
        
        elif value == "alter":
        
            
            if 0:
                print("Which attribute?\n")
                print("- name")
                print("- mass")
                print("- radius")
                print("- aphelion/apogee")
                print("- velocity (at aphelion/apogee)")
                print("- inclination")
                print("- color\n")
            
            curr_system.print_attr()
            
            attr = input("    > ")
            
            # similar as base loop but for attributes
            
            if attr == "name":
                curr_system.name = input("Give new name\n    > ")
            elif attr == "mass":
                curr_system.mass = float(input("Give new mass\n    > "))
            elif attr == "radius":
                curr_system.r = float(input("Give new radius\n    > "))
            elif attr == "aphelion" or attr == "apogee":
                curr_system.update_loc(float(\
                    input("Give new aphelion/apogee\n    > ")),curr_system.loc_p)
            elif attr == "velocity":
                curr_system.update_v_a(float(input("Give new velocity\n    > "))\
                    ,curr_system.v_p)
            elif attr == "inclination":
                curr_system.incl = float(input("Give new inclination\n    > "))
                curr_system.update_incl()
            elif attr == "color":
                c = input("Give new color\n    > ").split()
                curr_system.color = [float(c[0]),float(c[1]),float(c[2])]
            
        
        elif value == "up":
            orb = orb[:-1]
            
        elif value == "down":
            if len(curr_system.orbiters) == 0:
                print("No orbitors under!")
            
            else:
                print("Which orbiter?")
                curr_system.print_names_under()
                orb_name = input("    > ")
                
                b_found = False
                i = 0
                for orbiter in curr_system.orbiters:
                    
                    if orbiter.name == orb_name:
                        orb.append(i)
                        b_found = True
                        break
                    
                    i = i+1
                
                if not b_found:
                    print("No such orbitor")
            
        
        elif value == "save":
            
            filename = input("\nInsert filename\n    > ")
            write_h5_data(filename, system)
        elif value == "run":
            
            if len(orb) == 0:
                simulate(curr_system)
            
            else:
                # whether to simulate from curent object down or the whole system
                while not(value == "y" or value == "n"):
                    value = input("Simulate current partial system? (y/n)\n    > ")
                    
                    if value == "n":
                        simulate(system)
                    elif value == "y":
                        simulate(curr_system)
            
        
def simulate(system):
    """
    simulates the system with default paramters or inputted parameters
    
    :param: system         system to be simulated
    """
    
    # default settings or not
    vanilla = ""
    while not(vanilla == "y" or vanilla == "n"): 
        vanilla = input("Would you like to specify simulation settings?(y/n)\n    > ")
    
    # default settings
    time_step = 0.0001
    T_max = 100
    refresh_rate = 100
    update_time  = 60
    b_draw_orbit = 1
    b_contr = 1
    b_names = 0
    
    # alter the settings
    if vanilla == "y":
        time_step = float(input("Give time-step for the simulation (yrs) (default=0.0001)\n    > "))
        T_max     = float(input("Give simulation time (yrs) (default=100)\n    > "))
        refresh_rate = float(input("Give refresh rate of the animation (animate nth frame) (default=100)\n    > "))
        update_time  = float(input("Give the update time of the animation (time between frames) (default=60)\n    > "))
        answer = ""
        while not(answer == "y" or answer == "n"): 
            answer = input("Draw the orbits of the planets aswell?(y/n)\n    > ")
        
        if answer == "n":
            b_draw_orbit = 0
        
        answer = ""
        while not(answer == "y" or answer == "n"): 
            answer = input("Draw system with proper distances and sizes?(y/n)\n    > ")
        
        if answer == "y":
            b_contr = 0
        
        answer = ""
        while not(answer == "y" or answer == "n"): 
            answer = input("Display names?(y/n)\n    > ")
        
        if answer == "y":
            b_names = 1
        
    
    print("\n------ STARTING SIMULATION ------")
    
    
    
    # how much time per one simulation round in yrs
    time_step = time_step*365*24*60*60
    T         = 0
    # end time in yrs
    T_max     = T_max*365*24*60*60
    
    # save the points into a dict to plot the trajectories
    global past_points
    past_points = dict()
    
    
    update_time  = 1/update_time
    
    # initalize figure
    fig = plt.figure()
    
    ax = fig.gca(projection='3d')
    
    # plot planets
    plot_system(ax,system,1,0,b_contr)
        
    
    # set axis limits
    ax_max = max([ax.get_ylim()[1], ax.get_xlim()[1], ax.get_zlim()[1]])
    ax_min = -ax_max
    #min([ax.get_ylim()[0], ax.get_xlim()[0], ax.get_zlim()[0]])
       
    ax.set_ylim(ax_min,ax_max)
    ax.set_xlim(ax_min,ax_max)
    ax.set_zlim(ax_min,ax_max)
    
    # this is for saving animation not used
    #camera = Camera(fig)
    
    if b_names:
        names = [system.name]
        for orb in system.orbiters:
            names.append(orb.name)
        ax.legend(names)
    
    k = 0
    
    # simulation loop
    while T < T_max:
        
        try:
            if k == refresh_rate:
                
                # plot planets
                plot_system(ax,system,1,0,b_contr)
                
                if b_names:
                    ax.legend(names)
                
                
                ax.set_ylim(ax_min,ax_max)
                ax.set_xlim(ax_min,ax_max)
                ax.set_zlim(ax_min,ax_max)
                
                #ax.view_init(azim=0, elev=90)
                
                # plot trajectories
                if T > 0 and b_draw_orbit:
                    for orb in system.orbiters:
                        pos = past_points[f'{orb.name}']
                        x = []
                        y = []
                        z = []
                        for value in pos:
                            x.append(value[0])
                            y.append(value[1])
                            z.append(value[2])
                        
                        color = [orb.color[0]**(4),orb.color[1]**(4),orb.color[2]**(4)]
                        ax.plot(x,y,z, color=color, Linewidth=0.3)
                
                ax.set_xlabel("x")
                ax.set_ylabel("y")
                ax.set_zlabel("z")
                #camera.snap()
                T = T + refresh_rate*time_step
                print(f"T   = {T/(365*24*60*60):.4f} yrs, prog = {T/T_max:.4f}")
                pause(update_time)
                plt.cla()
                k = 0
            
            k = k+1
            
            # update the systen one time-step forward
            obj_update(system,system,time_step)
        except KeyboardInterrupt:
            break
    
    #animation = camera.animate()
    
    # remove global variable
    del past_points
    plt.close()
    
    fig_energ = plt.figure()
    ax_energ  = fig_energ.add_subplot(111)
    T_energ   = np.linspace(0,T_max,len(system.pot))
    
    
    for orb in system.orbiters:
        ax_energ.plot(T_energ,orb.pot,color=orb.color,label=orb.name)
    
    ax_energ.legend()
    for orb in system.orbiters:
        ax_energ.plot(T_energ,orb.kin,color=orb.color,label=orb.name)
    
    plt.show()
    
    
    print("\n------ SIMULATION ENDED ------\n\n")
    



    

def obj_update(system,obj,dt):
    """
    recursively updates the whole system
    
    :param: system    whole system
    :param: obj       object to be updated
    :param: dt        time step
    """
    
    # potential
    potential = calc_potential(system,obj,[0,0,0])
    
    # use update method
    obj.update(potential,dt)
    
    # update all orbiters
    for orb in obj.orbiters:
        
        obj_update(system,orb,dt)
        

def calc_potential(system,obj,potential):
    """
    Calculates the potential the system causes to object
    
    :param: system         system which causes potential
    :param: obj            object onto which potential is caused to
    :param: potential      previous potential which is updated here
    """
    # gravitational potential
    G = 6.6743e-11
    
    # absolute difference
    R = np.sqrt( (system.loc[0]-obj.loc[0])**2 + \
                 (system.loc[1]-obj.loc[1])**2 + \
                 (system.loc[2]-obj.loc[2])**2)
                 
    if R > 0:
        
        const = float(G*system.mass/R**3)
        
        temp_pot = [const*(system.loc[0]-obj.loc[0]),\
                    const*(system.loc[1]-obj.loc[1]),\
                    const*(system.loc[2]-obj.loc[2])]
        
        potential = [potential[0]+temp_pot[0],\
                     potential[1]+temp_pot[1],\
                     potential[2]+temp_pot[2]]
    
    # all orbiters of system causing potential
    for orb in system.orbiters:
    
        potential = calc_potential(orb,obj,potential)
    
    return potential
    
    

def plot_system(ax,system,s_ratio,d,b_contr):
    """
    Plots the objects to axis
    
    :param: ax        axis onto objects are plotted
    :param: system    system to be plotted
    :param: s_ratio   size ratio not really used
    :param: d         distance in contracted system
    :param: b_contr   bool if plottinf contracted system
    """
    
    if system.level == 0:
        # plots the star
        pos = plot_sphere(ax,system,1/12,d,b_contr)
        
    else:
        # plots everything else
        pos = plot_sphere(ax,system,1,d,b_contr)
        
        # adds position to dict
        try:
            past_points[f'{system.name}'].append(pos)
        except:
            past_points[f'{system.name}'] = [pos]
    
    # adds distance to contracted system
    d_new = d + system.r/7
    i = 0
    
    # plots orbiters
    for orb in system.orbiters:
        plot_system(ax,orb,1,d_new,b_contr)
        d_new = d_new + orb.r*4.1
        
        # adds distance
        if i+1 < len(system.orbiters):
            d_new = d_new + system.orbiters[i+1].r*4.1
        i = i + 1
        

def plot_sphere(ax,obj,s_ratio,d,b_contr):
    """
    plots the sphere in question
    :param: ax        axis onto objects are plotted
    :param: system    system to be plotted
    :param: s_ratio   size ratio in contracted system
    :param: d         distance in contracted system
    :param: b_contr   bool if plottinf contracted system
    """
    
    if b_contr:
        # define sphere and position in contracted system
        u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
        pos = [d*obj.loc[0]/np.linalg.norm(obj.loc), \
               d*obj.loc[1]/np.linalg.norm(obj.loc), \
               d*obj.loc[2]/np.linalg.norm(obj.loc)]
        x = s_ratio*obj.r*np.cos(u)*np.sin(v) + pos[0]
        y = s_ratio*obj.r*np.sin(u)*np.sin(v) + pos[1]
        z = s_ratio*obj.r*np.cos(v)           + pos[2]
        
        ax.plot_wireframe(x, y, z, color=obj.color)
        return pos
    
    else:
        # define sphere
        u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
        x = obj.r*np.cos(u)*np.sin(v) + obj.loc[0]
        y = obj.r*np.sin(u)*np.sin(v) + obj.loc[1]
        z = obj.r*np.cos(v)           + obj.loc[2]
        
        ax.plot_wireframe(x, y, z, color=obj.color)
        return obj.loc
        
    

def create_orbiter(system):
    """
    Creates orbiter to system
    """
    b_succ = False
    while not b_succ:
        try:
            name = input("Give name of the orbiter\n    > ")
            mass = input("Give mass of the orbiter\n    > ")
            r    = input("Give radius of the orbiter\n    > ")
            a    = input("Give aphelion/apogee of the orbiter\n    > ")
            v_a  = input("Give velocity at aphelion/apogee of the orbiter\n    > ")
            incl = input("Give inclination of the orbiter\n    > ")
            c    = input("Give color of the orbiter as r g b\n    > ").split()
            c    = [float(c[0]),float(c[1]),float(c[2])]
            level = system.level + 1
            b_succ = True
        except:
            print("Some values were given wrongly")
            
    return planetary_object(name, mass, r, a, v_a, float(incl)+360*system.incl/2/np.pi, \
                            level,system.loc,system.v,c)

def create_system():
    """
    Creates the star of the system i.e. initializes it. 
    """
    b_succ = False
    while not b_succ:
        try:
            name = input("Give name of the star\n    > ")
            mass = input("Give mass of the star\n    > ")
            r    = input("Give radius of the star\n    > ")
            a    = input("Give aphelion/apogee of the star (if the only star give 0)\n    > ")
            v_a  = input("Give velocity at aphelion/apogee of the star (if not moving give 0)\n    > ")
            incl = input("Give inclination of the star (if not tilted trajectory give 0)\n    > ")
            c    = input("Give color of the star as r g b\n    > ").split()
            c    = [float(c[0]),float(c[1]),float(c[2])]
            level = 0
            b_succ = True
        except:
            print("Some values were given wrongly")
    
    return planetary_object(name, mass, r, a, v_a, incl, level, [0,0,0], [0,0,0], c)

def main():
    
    # if data is not found in data directory
    if len(os.listdir('data/') ) == 0:
        print("Directory is empty")
        
        system = create_system()
    
    # if data is found load some 
    else:
        b_loaded = False
        
        while not b_loaded:
            filename = input("Give name of the system to be loaded\n    > ")
            
            if os.path.exists("data/"+filename+".h5"):
                system = read_h5_file(filename)
                b_loaded = True
            elif filename == "new":
                system = create_system()
                b_loaded = True
            elif filename != "exit":
                print(filename+".h5 doesn't exist!\n")
            
            
            if filename == "exit":
                return 0
                
    # start simulation
    pre_simulation(system)
    
    
    

if __name__=='__main__':
    main()
