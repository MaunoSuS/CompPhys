""" 
-------- Project work 2 - warm up ------------
----- FYS-4096 - Computational Physics -----

interpolates given data on a line and saves that data. Also plots the 
interpolated line and original data.
Splie class file taken from the 3rd exercise set.
"""
import numpy as np
import matplotlib.pyplot as plt
from spline_class import *
import os
import os.path
import h5py

def write_data(filename, r_grid, input_data):
    """
    Writes to hdf5 file
    :param: filename    filename of hdf5 file
    :param: r_grid      grid in r direction
    :param: input_data  value at grid points
    """

    with h5py.File(filename, "w") as fid:
        r_grid = fid.create_dataset("r_grid", data=r_grid, dtype="f")
        data = fid.create_dataset("data", data=input_data, dtype="f")

def read_hdf5_file(filename):
    """
    Reads hdf5 file
    :param:  filename   filename of hdf5 file
    :return:            information in file
    """
    fid = h5py.File(filename, "r")

    return np.array(fid["data"]), np.array(fid["x_grid"]), np.array(fid["y_grid"])

def main():
    
    # filename of the
    filename = "warm_up_data.h5"
    
    # get data from input file
    data, x_grid, y_grid = read_hdf5_file(filename)
    
    
    # spline interpolation
    spl = spline(x=x_grid, y=y_grid, f=data, dims = 2)
    
    # line along which interpolation and comparing happens
    y = np.linspace(-1.5,1.02,100)
    x = np.linspace(-1.5,0.25,100)
    
    # interpolate data along the line
    int_data = np.zeros(x.size)
    r = np.zeros(x.size)
    r0 = [-1.5,-1.5]
    
    for i in range(np.size(x)):
        int_data[i] = spl.eval2d(x[i],y[i])
        r[i] = np.sqrt( (x[i]-r0[0])**2 + (y[i]-r0[1])**2)
    
    print(f"works if interpolated value at r = [0.1, 0.1] is close to 0.1618.\n",\
          f"interpolated value: {spl.eval2d(0.1,0.1)}")
    
    fig = [plt.figure(), plt.figure()]
    ax  = [fig[0].add_subplot(111),fig[1].add_subplot(111)]
    
    # plot the interpolated data on the line r
    ax[0].plot(r, int_data)
    
    # countour the original data with a colorbar
    Y, X = np.meshgrid(y_grid,x_grid)
    
    cs = ax[1].contourf(X, Y, data)
    fig[1].colorbar(cs, ax=ax[1], shrink=0.9)
    ax[1].plot(x, y)
    
    plt.show()
    
    filename = "warm_up_interpolated.h5"
    # save the data for my colleague
    write_data(filename, r, int_data)
    

if __name__=='__main__':
    main()
