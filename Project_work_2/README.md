----------------- PROJECT WORK 2 ------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


warmup: interpolates given data on a line and saves that data. Also plots the 
        interpolated line and original data.
        Splie class file taken from the 3rd exercise set.
        
solar system simulation:
    dependencies:
    numpy
    matplotlib
    deepdish
    celluloid
    
    Simulates any starsystem with newtons gravitational law and small time
    steps. Thought about implementing some relativistic effect but it is 
    smaller than errors in the simulation.
    
    The simulation contains already the inner solar system, solar system planets
    and Pluto and solar system with largest dwarf planets and asteroids.
    
    If accurate aphelion or aphelion speeds were not found online they were
    calculated from geometry of ellipses and ellipse orbital mechanics.
    
    aphelion = a(1+e)  where a is semi major axis and e is eccentricity
    v = sqrt(GM(2/r-1/a)) where r is distance ie aphelion and G gravitational
                          constant and M is the mass of the star
    
    Notice that solarextended system has Sedna and it orbits so slowly that
    it is not really noticable. Thus remove it before simulation. It is almost
    900 AU away.
    
    To use run the program. Give either a name without the ".h" or to create
    new system type "new". After that commands should be shown as needed. At
    simulation loop CTRL+C stops the loop and reverts back to Text UI.
    
    systems in data:
    innersystem
    solar
    solarextended
    trappist (note that every other orbital period is about integer multiple of
              the orbital period of the inner most planet (atleast somewhat 
              close))
    Jupiter  (the star here is Jupiter and the rest are its orbiters. Didn't
              have time to add all objects)
    
          
