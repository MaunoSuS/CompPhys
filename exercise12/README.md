----------------- EXERCISESET 11 ------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: Added basic structure to molecular dynamics code with Morse potential.
          plottet enegergetics as function of time.
          
            Default settings:
            update/integration:  velocity verlet
            boundary conditions: (-inf,inf)
            dimensions:          3D
          
                     
problem2: Added  observables to simulation and plotting as function of time.
            Standard deviation of temperature is 65.662112

            " output
            E_kin 0.0008774475995447295
            E_pot -0.17361663274447348
            E_tot -0.17273918514492875

            Temperature 92.557764 +/- 65.662112
            Distance 1.407036871517909
            Heat capacity 25.803503353336094
            Heat capacity at average Temperature 7.483064952287036
            "
          
problem3: Calculated minimum of Lennar Jones potential and then implemented it
          to the simulation. Compared distance dependent energetics against 
          Morse potential.
            " output
            E_kin 0.009210549438214016
            E_pot -0.16458928582610105
            E_tot -0.15537873638788705

            Temperature 971.576945 +/- 712.600085
            Distance 1.4271045780797742
            Heat capacity 19.9498098551776
            Heat capacity at average Temperature 0.060182981952716055
            "
          

problem4: Initial temperature dependance of observables.
          
