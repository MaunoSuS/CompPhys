"""
Simple Molecular Dynamics code for course FYS-4096 Computational Physics

Problem 1:
- Make the code to work and solve H2 using the Morse potential.
- Modify and comment especially at parts where it reads "# ADD"
- Follow instructions on ex12.pdf

Default settings:
update/integration:  velocity verlet
boundary conditions: (-inf,inf)
dimensions:          3D

Problems 2:
- Add observables: temperature, distance, and heat capacity.
- Follow instructions on ex12.pdf

Standard deviation of temperature is 65.662112

" output
E_kin 0.0008774475995447295
E_pot -0.17361663274447348
E_tot -0.17273918514492875

Temperature 92.557764 +/- 65.662112
Distance 1.407036871517909
Heat capacity 25.803503353336094
Heat capacity at average Temperature 7.483064952287036
"

Problem 3:
- Add Lennard-Jones capability etc.
- Follow instructions on ex12.pdf

"output
E_kin 0.009210549438214016
E_pot -0.16458928582610105
E_tot -0.15537873638788705

Temperature 971.576945 +/- 712.600085
Distance 1.4271045780797742
Heat capacity 19.9498098551776
Heat capacity at average Temperature 0.060182981952716055
"

Problem 4:
- Temperature dependent data and analysis
- Follow instructions on ex12.pdf

"output
E_kin 0.0008774475995447295
E_pot -0.17361663274447348
E_tot -0.17273918514492875

Temperature 92.557764 +/- 65.662112
Distance 1.407036871517909
Heat capacity 25.803503353336094
Heat capacity at average Temperature 7.483064952287036
"

"""




from numpy import *
from matplotlib.pyplot import *

class Atom:
    def __init__(self,index,mass,dt,dims):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        self.LJ_epsilon = None
        self.LJ_sigma = None
        self.R = None
        self.v = None
        self.force = None

    def set_LJ_parameters(self,LJ_epsilon,LJ_sigma):
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self,coordinates):
        self.R = coordinates
        
    def set_velocity(self,velocity):
        self.v = velocity

    def set_force(self,force):
        self.force = force

class Observables:
    def __init__(self):
        self.E_kin = []
        self.E_pot = []
        self.distance = []
        self.Temperature = []
        self.heat_capacity = []

def calculate_energetics(atoms):
    """
    Sum over all atoms and calculate the energies for each
    """
    N = len(atoms)
    V = 0.0
    E_kin = 0.0
    
    # Kinetic:
    for i in range(N):
        E_kin += 1/2*atoms[i].mass*linalg.norm(atoms[i].v)**2

    # Potential:
    for i in range(0,N-1):
        for j in range(i+1,N):
            V += pair_potential(atoms[i], atoms[j])
    return E_kin, V

def calculate_force(atoms):
    """
    Calulates forces between every particle
    """
    N = len(atoms)
    ij_map = zeros((N,N),dtype=int)
    Fs = []
    ind = 0
    
    # Forces between two atoms
    for i in range(0,N-1):
        for j in range(i+1,N):
            Fs.append(pair_force(atoms[i],atoms[j]))
            ij_map[i,j] = ind
            ij_map[j,i] = ind
            ind += 1
    F = []
    # Sum the forces on an atom 
    for i in range(N):
        f = zeros(shape=shape(atoms[i].R))
        for j in range(N):
            ind = ij_map[i,j]
            if i<j:
                f += Fs[ind]
            elif i>j:
                f -= Fs[ind]
        F.append(f)
    F = array(F)
    return F

def pair_force(atom1,atom2):
    if b_LJ:
        return lennard_jones_force(atom1,atom2)
    else:
        return Morse_force(atom1,atom2)

def pair_potential(atom1,atom2):
    if b_LJ:
        return lennard_jones_potential(atom1,atom2)
    else:
        return Morse_potential(atom1,atom2)

def Morse_potential(atom1,atom2):
    # H2 parameters given here
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = atom1.R-atom2.R
    dr = sqrt(sum(r**2))
    
    # return the Morse potenetial as given in week 11 notes
    return De*(1 - exp(-a*(dr-re)))**2 - De

def Morse_force(atom1,atom2):
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    R = atom1.R - atom2.R
    r = sqrt(sum((atom1.R - atom2.R) ** 2))
    # by differentiating the Morse potential we get a directional force
    return -R/r*2*a*De * (exp(-a*(r-re)) - exp(-2*a*(r-re)))

def lennard_jones_potential(atom1,atom2):
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    r = sqrt(sum((atom1.R - atom2.R) ** 2))
    # Lennarr Jones potential as in lecture notes
    return 4*epsilon*( (sigma/r)**12 - (sigma/r)**6 )

def lennard_jones_force(atom1,atom2):
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    r = sqrt(sum((atom1.R - atom2.R) ** 2))
    R = atom1.R-atom2.R
    # negative of derivative of Lennard Jones potential as solved in
    # Ex12p3.pdf
    return -4*epsilon*( -12*sigma**12/r**13 + 6*sigma**6/r**7 )*R/r

def velocity_verlet_update(atoms):
    """
    Update positions, forces and velocities after time step dt according to 
    lecture notes on verlet method
    """
    dt = atoms[0].dt
    dt2 = dt**2
    for i in range(len(atoms)):
        # changes location of particles
        atoms[i].R += dt*atoms[i].v + dt2/(2*atoms[i].mass)*atoms[i].force
    # new forces on new locations
    Fnew = calculate_force(atoms)
    for i in range(len(atoms)):
        # new velocities and forces for particles
        atoms[i].v += dt/(2*atoms[i].mass)*(Fnew[i]+atoms[i].force)
        atoms[i].force = Fnew[i] # update force
    return atoms
    
def initialize_positions(atoms):
    # diatomic case
    atoms[0].set_position(array([-0.8,0.0,0.0]))
    atoms[1].set_position(array([0.7,0.0,0.0]))

def initialize_velocities(atoms):
    # vmax is the initial maximum based on kinetic energy
    # diatomic case
    dims = atoms[0].dims
    kB=3.16e-6 # in hartree/Kelvin
    for i in range(len(atoms)):
        v_max = sqrt(dims/atoms[i].mass*kB*T_init/2)
        atoms[i].set_velocity(array([1.0,0.0,0.0])*v_max)
    atoms[1].v = -1.0*atoms[0].v

def initialize_force(atoms):
    F=calculate_force(atoms)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])

def Temperature(E_k, atoms):
    # Boltzmann constant in Hartree/Kelvin
    kB = 3.16e-6
    # Temperature from notes
    return 2*E_k / (atoms[0].dims*len(atoms)*kB)

def calculate_observables(atoms,observables):
    E_k, E_p = calculate_energetics(atoms)
    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)
    T = Temperature(E_k, atoms)
    observables.Temperature.append(T)
    
    # average distance between all atoms
    d = 0
    k = 0
    for i in range(len(atoms)):
        for j in range(i+1, len(atoms)):
            d += sqrt(sum((atoms[i].R - atoms[j].R)**2))
            k += 1
    
    observables.distance.append(d/k)
    
    # Heat capacity:
    # boltzmann constant in hartree
    kB = 3.16e-6
    c = (mean((E_p + E_k)**2) -  mean(E_p + E_k)) / (kB*T**2)
    observables.heat_capacity.append(c)
    return observables

def main():
    N_atoms = 2
    dims = 3
    dt = 0.1
    time = linspace(0,0.1*100000,10000)
    mass = 1860.0
    
    global b_LJ 
    global T_init
    T_init = 20
    b_LJ = True
    
    # Initialize atoms
    atoms = []    
    for i in range(N_atoms):
        atoms.append(Atom(i,mass,dt,dims))
        atoms[i].set_LJ_parameters(0.1745,1.25)
    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms)
    initialize_force(atoms)

    for i in range(100000):
        atoms = velocity_verlet_update(atoms)
        if ( i % 10 == 0):
            observables = calculate_observables(atoms,observables)            
    
    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    T = array(observables.Temperature)
    C = array(observables.heat_capacity)
    d = array(observables.distance)
    
    fig = [0,0]
    ax = [0,0,0,0]
    
    # prob 1 figure
    fig[0] = figure()
    # prob 2 figure
    fig[1] = figure()
    
    # prob 1 axis
    ax[0] = fig[0].add_subplot(111)
    
    # prob 2 axis
    ax[1] = fig[1].add_subplot(311)
    ax[2] = fig[1].add_subplot(312)
    ax[3] = fig[1].add_subplot(313)
    
    # prob 1 plot
    ax[0].plot(time,E_kin, label="Kinetic")
    ax[0].plot(time,E_pot, label="Potential")
    ax[0].plot(time,E_pot+E_kin, label="Total")
    
    ax[0].legend()
    
    fig[0].suptitle("Problem 1")
    
    # prob 2 plot
    
    ax[1].plot(time,T, label="Temperature")
    ax[2].semilogy(time,C, label="Heat Capacity")
    ax[3].plot(time,d, label="interatomic distance")
    
    ax[2].set_ylim([1e-1,1e16])
    
    ax[1].set_title("Temperature")
    ax[2].set_title("Heat Capacity")
    ax[3].set_title("interatomic distance")
    
    for axe in ax:
        axe.label_outer()
    
    fig[1].suptitle("Problem 2")
    
    # Print energies
    print('E_kin',mean(E_kin))
    print('E_pot',mean(E_pot))
    print('E_tot',mean(E_kin)+mean(E_pot))
    print(f'\nTemperature {mean(T):.6f} +/- {std(T):.6f}')
    print('Distance', mean(d))
    print('Heat capacity', C[-1])
    print('Heat capacity at average Temperature', (mean((E_pot[-1]+ \
          E_kin[-1])**2)-mean(E_pot[-1]+E_kin[-1])) / (3.16e-6*mean(T)**2))
    
    # prob 3 calculations and plotting
    r1 = linspace(1.2,5,100)
    r2 = linspace(0.5,5,100)
    De = 0.1745
    re = 1.40
    a = 1.0282
    sigma = 1.25
    epsilon = 0.1745
    
    LJ_pot = 4*epsilon*((sigma/r1)**12 - (sigma/r1)**6)
    M_pot  = De * (1 - np.exp(-a * (r2 - re))) ** 2 - De
    
    fig.append(figure())
    
    ax.append(fig[-1].add_subplot(111))
    
    ax[-1].plot(r1,LJ_pot, label="Lennard Jones")
    ax[-1].plot(r2, M_pot, label="Morse")
    
    ax[-1].legend()
    fig[-1].suptitle("Problem 3")
    
    
    # prob 4
    
    T4 = linspace(100,400,10)
    
    fig.append(figure())
    ax.append(fig[-1].add_subplot(231))
    ax.append(fig[-1].add_subplot(232))
    ax.append(fig[-1].add_subplot(233))
    ax.append(fig[-1].add_subplot(234))
    ax.append(fig[-1].add_subplot(235))
    ax.append(fig[-1].add_subplot(236))
    
    #ax[-10].set_ylabel("Lennart Jones")
    #ax[-5].set_ylabel("Morse")
    ax[-6].set_title("Kinetic")
    ax[-5].set_title("Potential")
    ax[-4].set_title("Total")
    ax[-3].set_title("Distance")
    ax[-2].set_title("Heat Capacity")
    ax[-1].set_title("Observed Temperature with std")
    fig[-1].suptitle("Problem 4")
    
    for b_LJ in [False, True]:
        
        E_kin = []
        E_pot = []
        d = []
        C = []
        T_obs = []
        T_obs_std = []
        
        for T_init in T4:
            
            # Initialize atoms
            atoms = []    
            for i in range(N_atoms):
                atoms.append(Atom(i,mass,dt,dims))
                atoms[i].set_LJ_parameters(0.1745,1.25)
            # Initialize observables
            observables = Observables()

            # Initialize positions, velocities, and forces
            initialize_positions(atoms)
            initialize_velocities(atoms)
            initialize_force(atoms)

            for i in range(10000):
                atoms = velocity_verlet_update(atoms)
                if ( i % 10 == 0):
                    observables = calculate_observables(atoms,observables)            
            
            E_kin.append(mean(array(observables.E_kin)))
            E_pot.append(mean(array(observables.E_pot)))
            T_obs.append(mean(observables.Temperature))
            T_obs_std.append(std(observables.Temperature))
            C.append((mean((E_pot[-1]+E_kin[-1])**2)- \
                      mean(E_pot[-1]+E_kin[-1]))/(3.16e-6*mean(T_obs[-1])**2))
            d.append(mean(array(observables.distance)))
            
        if b_LJ:
            ax[-6].plot(T4,E_kin,label="Lennart Jones")
            ax[-5].plot(T4,E_pot)
            ax[-4].plot(T4,array(E_pot)+array(E_kin))
            ax[-3].plot(T4,d)
            ax[-2].semilogy(T4,C)
            ax[-1].plot(T4,T_obs,'b',label="Lennart Jones")
            ax[-1].plot(T4,array(T_obs)+array(T_obs_std),'r')
            ax[-1].plot(T4,array(T_obs)-array(T_obs_std),'r')
        else:
            ax[-6].plot(T4,E_kin,label="Morse")
            ax[-5].plot(T4,E_pot)
            ax[-4].plot(T4,array(E_pot)+array(E_kin))
            ax[-3].plot(T4,d)
            ax[-2].semilogy(T4,C)
            ax[-1].plot(T4,T_obs,'c',label="Morse")
            ax[-1].plot(T4,array(T_obs)+array(T_obs_std),'m')
            ax[-1].plot(T4,array(T_obs)-array(T_obs_std),'m')
            
    fig[-1].legend()
    show()

if __name__=="__main__":
    main()
        
