----------------- EXERCISESET 7------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: Solved 1D many interactiong electrons shrodinger equation with
          Hartree approximation
          
problem2: Saving simulation output to txt files and continuing from them
          
problem3: Saving simulation output to hdf5 files and continuing from them
          (note if threshold too small hdf5 rounds numbers such that loading
           from hdf5 files doesn't help much with speeding simulation.)

          When calling add arguments --ftype {txt or hdf5} if you want to load 
          from file if empty nothing loaded nothing saved
          and --th {threshold} to give threshold to the program
          
problem4: Hartree for six electrons and comparing interactinng and 
          noninteracting
