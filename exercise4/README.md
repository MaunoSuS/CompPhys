----------------- EXERCISESET 4------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: Played around with XCrySDen with given files. Made a few figures
          to show and calculated distances of some atoms and angles
          
problem2: Program calculates the number of electrons in given lattice cell.
          Also solves the reciprocal lattice vectors. Reads the data with
          given function in read_xsf_example.py file.
          
problem3: Calculate electron density along a given interpolation line
          
problem4: Calculate electron density along two given interpolation line
