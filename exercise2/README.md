----------------- EXERCISESET 2------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: Adding basis functions to modules according to lecture slides
          and adding commenting(lightly) to existing files spline_class,py
          and linear_interp.py. Created test functions for 1d 2d and 3d cases
          figures given by original code
problem2: Integrated given functions with simpson, trapezoid and monte-carlo 
          respectively (a,b,c) and comparing to answers given by reputable 
          software (MatLab, WolframAlpha)
problem3: Created N-dimensional numerical gradient to num_calculus.py (copied
          within this directory) and tested it. Created a steepest descent based
          function to find minima of well behaved functions
problem4: Created an algorithm to find a starting point to root searching
          as linear and exponential grid
