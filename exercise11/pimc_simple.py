
from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf

class Walker:
    """
    Objects that are created as this class store all the necessary information
    for solving the system with monte carlo method. Also some variables for
    optimization
    """
    def __init__(self,*args,**kwargs):
        # number of electrons
        self.Ne = kwargs['Ne']
        # position of electrons
        self.Re = kwargs['Re']
        # spins as 0 and 1
        self.spins = kwargs['spins']
        # number of nuclei
        self.Nn = kwargs['Nn']
        # position of nuclei
        self.Rn = kwargs['Rn']
        # charge of nuclei
        self.Zn = kwargs['Zn']
        # tau = beta/M
        self.tau = kwargs['tau']
        # dimensions
        self.sys_dim = kwargs['dim']

    def w_copy(self):
        # returns values
        return Walker(Ne=self.Ne,
                      Re=self.Re.copy(),
                      spins=self.spins.copy(),
                      Nn=self.Nn,
                      Rn=self.Rn.copy(),
                      Zn=self.Zn,
                      tau=self.tau,
                      dim=self.sys_dim)
    

def kinetic_action(r1,r2,tau,lambda1):
    """
    potential energy for walker action
    """
    return sum((r1-r2)**2)/lambda1/tau/4

def potential_action(Walkers,time_slice1,time_slice2,tau):
    """
    potential energy approximation for walker action
    """
    
    return 0.5*tau*(potential(Walkers[time_slice1]) \
                        +potential(Walkers[time_slice2]))

def pimc(Nblocks,Niters,Walkers):
    """
    uses path integral monte carlo to calculate energies and wave functions
    
    :param: Nblocks    number of blocks of iterations
    :param: Niters     number of iterations in block
    :param: Walkers    initial walkers that setup the system
    
    :return: Walkers   changed walkers
    :return: Eb        Energies for each block and the corresponding 
                       wavefunction
    :return: Accept    acceptance ratio for mmc iteration results
    """
    
    # number of points in "imaginary" path
    M = len(Walkers)
    Ne = Walkers[0].Ne*1
    #Nn = Walkers[0].Nn*1
    sys_dim = 1*Walkers[0].sys_dim
    # imaginary time-step
    tau = 1.0*Walkers[0].tau
    # constant for kinetic terms
    lambda1 = 1/(2*1836)
    Eb = zeros((Nblocks,))
    r = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))
    # variance for gauss distributed random movement
    sigma2 = lambda1*tau
    # std ^
    sigma = sqrt(sigma2)

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            # random time in range [0, M]
            time_slice0 = int(random.rand()*M)
            # two consecutive timesteps (or roll over to start of [0,M])
            time_slice1 = (time_slice0+1)%M
            time_slice2 = (time_slice1+1)%M
            # random electron to move
            ptcl_index = int(random.rand()*Ne)

            # reference position and positions to move
            r0 = Walkers[time_slice0].Re[ptcl_index]
            r1 = 1.0*Walkers[time_slice1].Re[ptcl_index]
            r2 = Walkers[time_slice2].Re[ptcl_index]
            
            # action before moving
            KineticActionOld = kinetic_action(r0,r1,tau,lambda1) +\
                kinetic_action(r1,r2,tau,lambda1)
            PotentialActionOld = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            # bisection sampling
            # r02_ave = (r0+r2)/2
            #log_S_Rp_R = -sum((r1-r02_ave)**2)/2/sigma2             
            #Rp = r02_ave + random.randn(sys_dim)*sigma
            #log_S_R_Rp = -sum((Rp - r02_ave)**2)/2/sigma2
            
            
            Rp, log_S_Rp_R, log_S_R_Rp = bisection_gauss_move(r0, r1, r2, sys_dim, sigma, sigma2)
            
            

            
            # save new position and calculate new action function values
            Walkers[time_slice1].Re[ptcl_index] = 1.0*Rp
            KineticActionNew = kinetic_action(r0,Rp,tau,lambda1) +\
                kinetic_action(Rp,r2,tau,lambda1)
            PotentialActionNew = potential_action(Walkers,time_slice0,time_slice1,tau)+potential_action(Walkers,time_slice1,time_slice2,tau)

            # action difference
            deltaK = KineticActionNew-KineticActionOld
            deltaU = PotentialActionNew-PotentialActionOld
            #print('delta K', deltaK)
            #print('delta logS', log_S_R_Rp-log_S_Rp_R)
            #print('exp(dS-dK)', exp(log_S_Rp_R-log_S_R_Rp-deltaK))
            #print('deltaU', deltaU)
            
            # improvement ratio and acceptance
            q_R_Rp = exp(log_S_Rp_R-log_S_R_Rp-deltaK-deltaU)
            A_RtoRp = min(1.0,q_R_Rp)
            
            # check acceptance against random number
            if (A_RtoRp > random.rand()):
                Accept[i] += 1.0
            else:
                # if not accepted return to original pos
                Walkers[time_slice1].Re[ptcl_index]=1.0*r1
            # acceptance count for a block
            AccCount[i] += 1
            
            # calculate energy every interval
            if j % obs_interval == 0:
                E_kin, E_pot = Energy(Walkers)
                #print(E_kin,E_pot)
                Eb[i] += E_kin + E_pot
                EbCount += 1
            #exit()
        
        for index in range(len(Walkers)):
            r[i] += sqrt(sum((Walkers[index].Re[0] - Walkers[index].Re[1]) ** 2))
        
        # final energies and acceptance and distance
        r[i] /= M
        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept, r


def Energy(Walkers):
    """
    Energy of hydrogen as average of walkers
    """
    M = len(Walkers)
    d = 1.0*Walkers[0].sys_dim
    tau = Walkers[0].tau
    lambda1 = 1/(2*1836)
    U = 0.0
    K = 0.0
    for i in range(M):
        U += potential(Walkers[i])
        for j in range(Walkers[i].Ne):
            if (i<M-1):
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[i+1].Re[j])**2)/4/lambda1/tau**2
            else:
                K += d/2/tau-sum((Walkers[i].Re[j]-Walkers[0].Re[j])**2)/4/lambda1/tau**2    
    return K/M,U/M
        

def bisection_gauss_move(r0, r1, r2, sys_dim, sigma, sigma2):
    # bisection sampling (as in dissertation)
    r02_ave = (r0 + r2) / 2
    log_S_Rp_R = -sum((r1 - r02_ave) ** 2) / 2 / sigma2  # Gauss sampling probability
    # Gauss distributed random move of r1 in regards to the reference
    # "bisection" of r0 and r2:
    Rp = r02_ave + random.randn(sys_dim) * sigma
    log_S_R_Rp = -sum((Rp - r02_ave) ** 2) / 2 / sigma2  # Gauss sampling probability

    return Rp, log_S_Rp_R, log_S_R_Rp

def potential(Walker):
    V = 0.0
    
    # Morse potential:
    D = 0.1745
    re = 1.40
    a = 1.0282

    # Morse
    for i in range(Walker.Ne-1):
        for j in range(i+1,Walker.Ne):
            r = sqrt(sum((Walker.Re[i]-Walker.Re[j])**2))
            V += D*(1-exp(-a*(r-re)))**2 - D

    return V  


def main():

    figs = [figure()]
    
    axes = [figs[0].add_subplot(231),
            figs[0].add_subplot(234),
            figs[0].add_subplot(232),
            figs[0].add_subplot(235),
            figs[0].add_subplot(233),
            figs[0].add_subplot(236)]
            
    final_msg = ""
    k = 0
    
    # calculates internuclear distence and energetics for three different
    # Trotter numbers
    for M in [1,8,16]:
        
        Walkers=[]
        
        """
        # For H2
        Walkers.append(Walker(Ne=2,
                              Re=[array([0.5,0,0]),array([-0.5,0,0])],
                              spins=[0,1],
                              Nn=2,
                              Rn=[array([-0.7,0,0]),array([0.7,0,0])],
                              Zn=[1.0,1.0],
                              tau = 1/(300*M*3.16681e-6), # Kb=3.16681e-6 in Hartree units
                              dim=3))
        
        """
        # For 2D quantum dot
        Walkers.append(Walker(Ne=2,
                              Re=[array([0.5,0,0]),array([-0.5,0,0])],
                              spins=[0,0],
                              Nn=2, # not used
                              Rn=[array([-0.7,0]),array([0.7,0])], # not used
                              Zn=[1.0,1.0], # not used
                              tau = 1/(300*M*3.16681e-6), # Kb=3.16681e-6 in Hartree units
                              dim=3))
       
       
        for i in range(M-1):
             Walkers.append(Walkers[i].w_copy())
        Nblocks = 200
        Niters = 100
        
        Walkers, Eb, Acc, r = pimc(Nblocks,Niters,Walkers)
        
        
        conv_cut=20
        
        axes[2*k].set_title(f"M = {M}")
        
        axes[2*k].plot(Eb)
        #axes[2*k].plot([conv_cut,conv_cut],axes[2*k].get_ylim(),'k--')
        Eb = Eb[conv_cut:]
        axes[2*k].axhline(mean(Eb), c='r')
        
        axes[2*k+1].plot(r)
        #axes[2*k+1].plot([conv_cut,conv_cut],[1.3,1.61],'k--')
        r = r[conv_cut:]
        axes[2*k+1].axhline(mean(r), c='r')
               
        axes[2*k+1].set_xlabel('blocks')
        
        
        print('PIMC total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
        print('Variance to energy ratio: {0:.5f}'.format(abs(var(Eb)/mean(Eb)))) 
        
        final_msg += f'for M = {M}\n' \
                  +  f'PIMC total energy: {mean(Eb):.5f} +/- ' \
                  +  f'{std(Eb)/sqrt(len(Eb)):0.5f} \n' \
                  +  f'Variance to energy ratio: {abs(var(Eb)/mean(Eb)):.5f}\n\n'
        
        k += 1
        
        
    axes[0].set_ylabel('E/Hartree')
    axes[1].set_ylabel('r/a.u.')
    
    print(final_msg)
    
    k = 0
    while k < 3:
        axes[2*k].set_ylim(-0.177,-0.157)
        axes[2*k].label_outer()
        
        axes[2*k+1].set_ylim(1.3,1.61)
        axes[2*k+1].label_outer()
        k+=1
    
    figs[0].savefig(f'problem4.jpg')
    show()

if __name__=="__main__":
    main()
        
