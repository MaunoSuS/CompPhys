"""
Simple Monte Carlo for Ising model

Related to course FYS-4096 Computational Physics

Problem 1:
- Make the code to work, that is, include code to where it reads "# ADD"
- Comment the parts with "# ADD" and make any additional comments you 
  think could be useful for yourself later.
- Follow the assignment from ex11.pdf.

Problem 2:
- Add observables: heat capacity, magnetization, magnetic susceptibility
- Follow the assignment from ex11.pdf.

Problem 3:
- Look at the temperature effects and locate the phase transition temperature.
- Follow the assignment from ex11.pdf.


OBS! if you change Ntemp remove folder problem3

"""


from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf
import os
import os.path
import h5py

class Walker:
    def __init__(self,*args,**kwargs):
        self.spin = kwargs['spin']
        self.nearest_neighbors = kwargs['nn']
        self.sys_dim = kwargs['dim']
        self.coords = kwargs['coords']

    def w_copy(self):
        return Walker(spin=self.spin.copy(),
                      nn=self.nearest_neighbors.copy(),
                      dim=self.sys_dim,
                      coords=self.coords.copy())
    

def Energy(Walkers):
    E = 0.0
    J = 4.0 # given in units of k_B
    # ADD calculation of energy
    
    for walker in Walkers:
        E += site_Energy(Walkers,walker)
    
    # each neighbor connection is considered twice
    return E/2

def site_Energy(Walkers,Walker):
    # energy of nearest naighbor spins
    E = 0.0
    J = 4.0 # given in units of k_B
    for k in range(len(Walker.nearest_neighbors)):
        j = Walker.nearest_neighbors[k]
        E += -J*Walker.spin*Walkers[j].spin
    return E

def ising(Nblocks,Niters,Walkers,beta):
    M = len(Walkers)
    Eb = zeros((Nblocks,))
    Eb2 = zeros((Nblocks,))
    Mb = zeros((Nblocks,))
    Mb2 = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            site = int(random.rand()*M)

            s_old = 1.0*Walkers[site].spin
 
            E_old = site_Energy(Walkers,Walkers[site])
            
            # new spin as random
            s_new = 0.5 * (-1)**random.randint(0,2)

            Walkers[site].spin = 1.0*s_new

            E_new = site_Energy(Walkers,Walkers[site])

            # Metropolis Monte Carlo
            q = exp(-beta*(E_new-E_old))
            acpt = min(1.0,q)
            
            if acpt > random.rand():
                Accept[i] += 1.0
            else:
                Walkers[site].spin = s_old
            
            AccCount[i] += 1

            if j % obs_interval == 0:
                E_tot = Energy(Walkers)/M # energy per spin
                Eb[i] += E_tot
                EbCount += 1
                
                # for heat capacity and magnetization
                Eb2[i] += E_tot**2
                mag = magnetization(Walkers)
                Mb[i] += mag
                Mb2[i] += mag**2 
                
            
        Eb[i] /= EbCount
        Eb2[i] /= EbCount
        Mb[i] /= EbCount
        Mb2[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept, Eb2, Mb, Mb2

def magnetization(Walkers):
    # calculate the magnetization
    mag = 0.0
    for Walker in Walkers:
        mag += Walker.spin
    return mag

def heat_capacity(E, E2, T):
    # Calculates heat capacity
    return (np.mean(E2) - np.mean(E)**2) / (T**2)

def susceptibility(M, M2, T):
    return (np.mean(M2) - np.mean(M)**2) / T

def write_data(fname, Acc, Eb, Eb2, Mb, Mb2, eq, T, cv, sus):
    # write to hdf5

    with h5py.File(fname, "w") as f:
        Acc_set = f.create_dataset("Acc", data=Acc, dtype="f")
        Eb_set = f.create_dataset("Eb", data=Eb, dtype="f")
        Eb2_set = f.create_dataset("Eb2", data=Eb2, dtype="f")
        Mb_set = f.create_dataset("Mb", data=Mb, dtype="f")
        Mb2_set = f.create_dataset("Mb2", data=Mb2, dtype="f")
        T = f.create_dataset("T", data=T, dtype="f")
        eq = f.create_dataset("eq", data=eq, dtype="int")
        cv = f.create_dataset("cv", data=cv, dtype="f")
        sus = f.create_dataset("sus", data=sus, dtype="f")

def read_hdf5_file(fname):
    # Load from hdf5 file
    f = h5py.File(fname, "r")
    
    Acc = array(f["Acc"])
    Eb = array(f["Eb"])
    Eb2 = np.array(f["Eb2"])
    Mb = np.array(f["Mb"])
    Mb2 = np.array(f["Mb2"])
    T = np.array(f["T"])
    eq = np.array(f["eq"])
    cv = np.array(f["cv"])
    sus = np.array(f["sus"])
    
    f.close()
    return Acc, Eb, Eb2, Mb, Mb2, eq, T, cv, sus

def calculate(dim, grid_side, grid_size, Nblocks, Niters, T, beta):
    Walkers=[]

    
    # Ising model nearest neighbors only
    mapping = zeros((grid_side,grid_side),dtype=int) # mapping
    inv_map = [] # inverse mapping
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i,j]=ii
            inv_map.append([i,j])
            ii += 1
     
    # create walkers and assign neighbor for each
    for i in range(grid_side):
        for j in range(grid_side):
            # periodic boundary condition
            j1=mapping[i,(j-1) % grid_side]
            j2=mapping[i,(j+1) % grid_side]
            i1=mapping[(i-1) % grid_side,j]
            i2=mapping[(i+1) % grid_side,j]
            Walkers.append(Walker(spin=0.5,
                                  nn=[j1,j2,i1,i2],
                                  dim=dim,
                                  coords = [i,j]))
     
   
    return ising(Nblocks,Niters,Walkers,beta)
    
    
def main():
    
    dim = 2
    grid_side = 10
    grid_size = grid_side**dim
    
    Nblocks = 200
    Niters = 1000
    eq = 20 # equilibration "time"
    
    """
    Notice: Energy is measured in units of k_B, which is why
            beta = 1/T instead of 1/(k_B T)
    """
    
    # ------------ PROBLEM 2 ----------------
    print("\n")
    print("------------------ PROBLEM2 -------------------------")
    
    T = 3.0
    beta = 1.0/T
        
    if os.path.exists("problem2.hdf5"):
        print("load data from file")
        Acc, Eb, Eb2, Mb, Mb2, eq, T, cv, sus = read_hdf5_file("problem2.hdf5")
    else:
        Walkers, Eb, Acc, Eb2, Mb, Mb2 = calculate(dim, grid_side, grid_size, \
                                         Nblocks, Niters, T, beta)
        sus = susceptibility(Mb[eq:], Mb2[eq:], T)
        cv = heat_capacity(Eb[eq:], Eb2[eq:], T)
        
    
    figs = [figure(), figure()]
    axes = [figs[0].add_subplot(211), 
            figs[0].add_subplot(212),
            figs[1].add_subplot(411),
            figs[1].add_subplot(412),
            figs[1].add_subplot(413),
            figs[1].add_subplot(414),]
    
    axes[0].plot(Eb)
    axes[1].plot(Mb)
    
    axes[0].set_title("Energy")
    axes[1].set_title("Magnetization")
    
    axes[0].set_ylabel("E")
    axes[1].set_ylabel("M")
    axes[1].set_xlabel("Block")
    figs[0].suptitle("Problem2")
    
    for ax in axes:
        ax.label_outer()
    
    if not os.path.exists("problem2.hdf5"):
        write_data("problem2.hdf5", Acc, Eb, Eb2, Mb, Mb2, eq, T, cv, sus)

    
    # ------------------ PROBLEM3 -------------------------
    print("\n")
    print("------------------ PROBLEM3 -------------------------")
    
    Ntemp = 100
    i = 0
    sus3 = zeros(Ntemp)
    cv3 = zeros(Ntemp)
    E3 = zeros(Ntemp)
    M3 = zeros(Ntemp)
    Temp3 = linspace(0.5,6.0, num=Ntemp)
    
    # save data to a folder so do not have to run simulation every time
    if not os.path.exists("problem3"):
        os.mkdir("problem3")
        print("created folder problem3")
    
    # does the same thing as in problem2 but for multiple temperatures and then
    # plots temperature dependencies
    for T3 in Temp3:
        
        if os.path.exists(f"problem3/problem3_{i}.hdf5"):
            print("load data from file")
            Acc3, Eb3, Eb23, Mb3, Mb23, eq3, T3, cv3, sus3 = read_hdf5_file(\
                                                 f"problem3/problem3_{i}.hdf5")
        else:
            
            beta3 = 1/T3
            Walkers3, Eb3, Acc3, Eb23, Mb3, Mb23 = calculate(dim, grid_side, \
                                          grid_size,Nblocks, Niters, T3, beta3)
                                         
        E3[i] = mean(Eb3[eq:])
        M3[i] = mean(Mb3[eq:])
        sus3[i] = susceptibility(Mb3[eq:], Mb23[eq:], T3)
        cv3[i] = heat_capacity(Eb3[eq:], Eb23[eq:], T3)
        
        if not os.path.exists(f"problem3/problem3_{i}.hdf5"):
            write_data(f"problem3/problem3_{i}.hdf5", Acc3, Eb3, Eb23, Mb3, \
                       Mb23, eq, T3, cv3, sus3)
        
        i += 1
    
    axes[2].plot(Temp3,E3)
    axes[3].plot(Temp3,cv3)
    axes[4].plot(Temp3,M3)
    axes[5].plot(Temp3,sus3)
    
    axes[2].set_ylabel("Energy per spin")
    axes[3].set_ylabel("Heat Capacity per spin")
    axes[4].set_ylabel("Magnetization per spin")
    axes[5].set_ylabel("Susceptibility per spin")
    axes[5].set_xlabel("T/K")
    figs[1].suptitle("Problem3")
    
    axes[3].yaxis.tick_right()
    axes[5].yaxis.tick_right()
    axes[3].yaxis.set_label_position("right")
    axes[5].yaxis.set_label_position("right")
    
    print("\n")
    
    # print output of PROBLEM 2
    print('Ising total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb[eq:]), \
          std(Eb[eq:])/sqrt(len(Eb[eq:]))))
    print('Variance to energy ratio: {0:.5f}'.format(abs(var(Eb[eq:]) \
          /mean(Eb[eq:])))) 
    
    print(f"Heat capacity {cv:.5f}")
    
    print(f"Susceptibility {sus:.5f}")
    
    show()

if __name__=="__main__":
    main()
        
