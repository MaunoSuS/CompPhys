----------------- EXERCISESET 11 ------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: Added the Metropolis Monte Carlo algorithm for flipping a spin
          
          settings of simulation:
          T = 3 K
          dim = 2
          grid_side = 10x10
          periodic boundary conditions with considering only nearest neighbors
          exchange constant is 4
          
                     
problem2: Added  calculation of heat capacity, magnetization and magnetic 
          susceptibility in to the code.
          
          output is now:
          Ising total energy: -0.84863 +/- 0.01112
          Variance to energy ratio: 0.02621
          Heat capacity 0.00447
          Susceptibility 92.74424
          
          Also added data writing / reading functions (hdf5)
          (If data exists use it as results instead of simulating again)

          
problem3: Calculated observables with different temperatures and plotted
          the temperature dependency of them.
          
          Same data writing / reading is used here so simulations are
          not necessary to do every time
          
          Phase transition happens around 2.5 K 
          (calculated with a ridiculous number of points for fun)
          
!!! The figures are not saved since the data is saved and is easily plotted !!!

problem4: Calculates internuclear distances and energetics for two hydrogen
          atoms in 3D with Morse potential energy surface for three different
          Trotter numbers using T=300K.
          
