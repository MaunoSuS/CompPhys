-------------- COMPUTAIONAL PHYSICS ---------------
                     FYS-4096

**************************************************
**            Matias Mustonen 267207            **
**************************************************

This directory contains all created for the course.
Each exercise set is in its own folder with all
needed files.

---------------------------------------------------
exercise1: setting up and first problems (differentials)
	       and convergences of numerical differentiation
	       and packaging of multifile project
	  
exercise2: interpolation functions, using numerical integration 
           numerical N-dimensional gradient, steepest descent and 
           algorithm for root search

exercise3: integration and interpolation once more, solving eigen
           values and vectors with powermethod and solving electric 
           field of 1-D rod
           
exercise4: Lattice structure visualization and lattice cell calculation.
           Also electron density in lattice cell.
           
exercise5: Solving partial differential equations evolution and trajectories
           of moving particles in fields. Also Runge-Kutta
           
exercise6: Solving Poisson equation and heat equation with finite element
           method and fenics. Also some review of first half of course
           
Project_work_1: warm up exercise 2d integration and quantum mechanical
                maxwell's demon project work
                
exercise7: 1D many body schrodinger equations with hartree approximation
           and also saving to file and continuing from previous simulations
           
exercise8: Hartree-Fock approximation for many-particle system, initalizing
           PUHTI "profile" and calculating potential energy surface of O2
           near equilibrium distance using quantum espresso. Also with quantum
           espresso calculated the convergence of diamond lattice energy 
           with respect to cut-off energy and lattice-grid (k-grid)
           
exercise9: Energy of hydrogen molecule using vmc and dmc. Energy of diamond
           lattice with vmc and dmc on puhti.
           
exercise10: Path integral monte carlo and periodic lattice eigen functions
            and energies. Also PIMC for many particle semiconductor and 
            graphene energies with vmc/dmc on PUHTI.

exercise11: Nearest neighbor spin model for ferromagnet with Metropolis
            Monte Carlo. From data calculates observables: energy, 
            magnetization, heat capacity and susceptibility. Also calculates the
            temperature dependency of observables. Includes data writing /
            reading and plotting. Additionally Morse potential surface
            calculation of interaction of two hydrogen.
            
exercise12: Molecular dynamics. Completed the given code. Added Velocity Verlet
            Observables and Lennard Jones potential. Solved the minimum of
            Lennard Jones also. Finally initial temperature dependance 
            of observables.

Project_work_2: warm up exercise interpolation of 2d data and loading and saving 
                h5-format. 
                
                The project itself is a solarsystem or rather gravitational 
                system simulator with intricate text UI to edit the simulation 
                itself. Contains by default inner solarsystem, solarsystem
                and solarsystem with some dwarf planets and asteroids
