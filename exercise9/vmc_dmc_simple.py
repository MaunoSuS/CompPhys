"""
Simple VMC and DMC code for FYS-4096 course at TAU

- Fill in more comments based on lecture slides
- Follow assignment instructions
-- e.g., the internuclear distance should be 1.4 instead of 1.5
   and currently a=1, but it should be 1.1 at first


By Ilkka Kylanpaa
"""

from numpy import *
from matplotlib.pyplot import *

class Walker:
    """
    Objects that are created as this class store all the necessary information
    for solving the system with monte carlo method. Also some variables for
    optimization
    """
    def __init__(self,*args,**kwargs):
        # number of electrons
        self.Ne = kwargs['Ne']
        # position of electrons
        self.Re = kwargs['Re']
        # spins as 0 and 1
        self.spins = kwargs['spins']
        # number of nuclei
        self.Nn = kwargs['Nn']
        # position of nuclei
        self.Rn = kwargs['Rn']
        # charge of nuclei
        self.Zn = kwargs['Zn']
        # dimensions
        self.sys_dim = kwargs['dim']

    def w_copy(self):
        # returns values
        return Walker(Ne=self.Ne,
                      Re=self.Re.copy(),
                      spins=self.spins.copy(),
                      Nn=self.Nn,
                      Rn=self.Rn.copy(),
                      Zn=self.Zn,
                      dim=self.sys_dim)
    

def vmc_run(Nblocks,Niters,Delta,Walkers_in,Ntarget):
    """
    uses variational monte carlo to calculate energies and wave functions
    
    :param: Nblocks    number of blocks of iterations
    :param: Niters     number of iterations in block
    :param: Delta      Size of random walker step
    :param: Walkers_in initial walkers that setup the system
    :param: Ntarget    target number of walkers
    
    :return: walkers_out   as many walkers as Ntarget allows
    :return: Eb            Energies for each block and the corresponding 
                           wavefunction
    :return: Accept        acceptance ratio for mmc iteration results
    """
    Eb = zeros((Nblocks,))
    Accept=zeros((Nblocks,))

    vmc_walker=Walkers_in[0] # just one walker needed
    Walkers_out=[]
    for i in range(Nblocks):
        for j in range(Niters):
            # moving only electrons
            for k in range(vmc_walker.Ne):
                R = vmc_walker.Re[k].copy()
                Psi_R = wfs(vmc_walker)

                # move the particle
                vmc_walker.Re[k] = R + Delta*(random.rand(vmc_walker.sys_dim)-0.5)
                
                # calculate wave function at the new position
                Psi_Rp = wfs(vmc_walker)

                # calculate the sampling probability
                A_RtoRp = min((Psi_Rp/Psi_R)**2,1.0)
                
                # Metropolis
                if (A_RtoRp > random.rand()):
                    Accept[i] += 1.0/vmc_walker.Ne
                else:
                    vmc_walker.Re[k]=R
                #end if
            #end for
            Eb[i] += E_local(vmc_walker)
        #end for
        if (len(Walkers_out)<Ntarget):
            Walkers_out.append(vmc_walker.w_copy())
        Eb[i] /= Niters
        Accept[i] /= Niters
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))
    
    return Walkers_out, Eb, Accept


def dmc_run(Nblocks,Niters,Walkers,tau,E_T,Ntarget):
    """
    uses diffusion monte carlo to calculate energies and wave functions
    
    :param: Nblocks    number of blocks of iterations
    :param: Niters     number of iterations in block
    :param: tau        initial diffusion step size
    :param: Walkers    initial walkers that setup the system
    :param: Ntarget    target number of walkers
    :param: E_T        prev calc ground state energy
    
    :return: walkers_out   as many walkers as Ntarget allows
    :return: Eb            Energies for each block and the corresponding 
                           wavefunction
    :return: Accept        acceptance ratio for mmc iteration results
    """
    max_walkers = 2*Ntarget
    lW=len(Walkers)
    
    # copy random walkers wanted number of times
    while len(Walkers)<Ntarget:
        Walkers.append(Walkers[max(1,int(lW*random.rand()))].w_copy())

    Eb=zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))
    
    obs_interval=5
    mass=1

    for i in range(Nblocks):
        EbCount=0
        for j in range(Niters):
            Wsize = len(Walkers)
            Idead = []
            for k in range(Wsize):
                Acc = 0.0
                for np in range(Walkers[k].Ne):
                    # electron pos
                    R = Walkers[k].Re[np].copy()
                    
                    # wavefunc
                    Psi_R = wfs(Walkers[k])
                    DriftPsi_R = 2*Gradient(Walkers[k],np)/Psi_R*tau/2/mass
                    E_L_R=E_local(Walkers[k])

                    DeltaR=random.randn(Walkers[k].sys_dim)
                    logGf=-0.5*dot(DeltaR,DeltaR)
                    
                    # random move walker and add drift from gradient
                    Walkers[k].Re[np]=R+DriftPsi_R+DeltaR*sqrt(tau/mass)
                    
                    # wavefunc and E at new pos
                    Psi_Rp = wfs(Walkers[k])
                    DriftPsi_Rp = 2*Gradient(Walkers[k],np)/Psi_Rp*tau/2/mass
                    E_L_Rp = E_local(Walkers[k])
                    
                    DeltaR = R-Walkers[k].Re[np]-DriftPsi_Rp
                    logGb = -dot(DeltaR,DeltaR)/2/tau*mass
                    
                    # acceptance with 2 green funcs
                    A_RtoRp = min(1, (Psi_Rp/Psi_R)**2*exp(logGb-logGf))
                    
                    # acceptance compared to rand number
                    if (A_RtoRp > random.rand()):
                        Acc += 1.0/Walkers[k].Ne
                        Accept[i] += 1
                    else:
                        Walkers[k].Re[np]=R
                    
                    AccCount[i] += 1
                
                tau_eff = Acc*tau
                GB = exp(-(0.5*(E_L_R+E_L_Rp) - E_T)*tau_eff)
                MB = int(floor(GB + random.rand()))
                
                # if walker is too far, KILL IT
                if MB>1:
                    for n in range(MB-1):
                        if (len(Walkers)<max_walkers):
                            Walkers.append(Walkers[k].w_copy())
                elif MB==0:
                    Idead.append(k)
 
            # delete dead walkers
            Walkers = DeleteWalkers(Walkers,Idead)

            # Calculate observables every now and then
            if j % obs_interval == 0:
                EL = Observable_E(Walkers)
                Eb[i] += EL
                EbCount += 1
                E_T += 0.01/tau*log(Ntarget/len(Walkers))
                

        Nw = len(Walkers)
        dNw = Ntarget-Nw
        for kk in range(abs(dNw)):
            ind=int(floor(len(Walkers)*random.rand()))
            if (dNw>0):
                Walkers.append(Walkers[ind].w_copy())
            elif dNw<0:
                Walkers = DeleteWalkers(Walkers,[ind])

        
        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept

def DeleteWalkers(Walkers,Idead):
    """
    Deletes dead walkers
    """
    if (len(Idead)>0):
        if (len(Walkers)==len(Idead)):
            Walkers = Walkers[0]
        else:
            Idead.sort(reverse=True)   
            for i in range(len(Idead)):
                del Walkers[Idead[i]]

    return Walkers

def H_1s(r1,r2):
    """
    approximate form of electron wave functions in hydrogen
    """
    return exp(-a*sqrt(sum((r1-r2)**2)))
     
def wfs(Walker):
    """
    wave-function for walker: hydrogen molecule with jastrow factor (electron-
    electron and electron-ion interactions)
    """
    # H2 approx
    f = H_1s(Walker.Re[0],Walker.Rn[0])+H_1s(Walker.Re[0],Walker.Rn[1])
    f *= (H_1s(Walker.Re[1],Walker.Rn[0])+H_1s(Walker.Re[1],Walker.Rn[1]))

    J = 0.0
    
    if Jastrow:
        # Jastrow e-e
        for i in range(Walker.Ne-1):
            for j in range(i+1,Walker.Ne):
               r = sqrt(sum((Walker.Re[i]-Walker.Re[j])**2))
               if (Walker.spins[i]==Walker.spins[j]):
                   J += 0.25*r/(1.0+1.0*r)
               else:
                   J += 0.5*r/(1.0+1.0*r)
        
        # Jastrow e-Ion
        for i in range(Walker.Ne):
            for j in range(Walker.Nn):
               r = sqrt(sum((Walker.Re[i]-Walker.Rn[j])**2))
               J -= Walker.Zn[j]*r/(1.0+100.0*r)
           

    return f*exp(J)

def potential(Walker):
    """
    potential energy for walker
    """
    V = 0.0
    r_cut = 1.0e-4
    # e-e
    for i in range(Walker.Ne-1):
        for j in range(i+1,Walker.Ne):
            r = sqrt(sum((Walker.Re[i]-Walker.Re[j])**2))
            V += 1.0/max(r_cut,r)

    # e-Ion
    for i in range(Walker.Ne):
        for j in range(Walker.Nn):
            r = sqrt(sum((Walker.Re[i]-Walker.Rn[j])**2))
            V -= Walker.Zn[j]/max(r_cut,r)

    # Ion-Ion
    for i in range(Walker.Nn-1):
        for j in range(i+1,Walker.Nn):
            r = sqrt(sum((Walker.Rn[i]-Walker.Rn[j])**2))
            V += 1.0/max(r_cut,r)

    return V

def Local_Kinetic(Walker):
    """
    kintetic energy for walker
    """
    # laplacian -0.5 \nabla^2 \Psi / \Psi
    h = 0.001
    h2 = h*h
    K = 0.0
    Psi_R = wfs(Walker)
    for i in range(Walker.Ne):
        for j in range(Walker.sys_dim):
            Y=Walker.Re[i][j]
            Walker.Re[i][j]-=h
            wfs1 = wfs(Walker)
            Walker.Re[i][j]+=2.0*h
            wfs2 = wfs(Walker)
            K -= 0.5*(wfs1+wfs2-2.0*Psi_R)/h2
            Walker.Re[i][j]=Y
    return K/Psi_R

def Gradient(Walker,particle):
    """
    gradient for walker
    """
    h=0.001
    dPsi = zeros(shape=shape(Walker.Re[particle]))
    for i in range(Walker.sys_dim):
        Y=Walker.Re[particle][i]
        Walker.Re[particle][i]-=h
        wfs1=wfs(Walker)
        Walker.Re[particle][i]+=2.0*h
        wfs2=wfs(Walker)
        dPsi[i] = (wfs2-wfs1)/2/h
        Walker.Re[particle][i]=Y

    return dPsi

def E_local(Walker):
    """
    total energy for walker
    """
    return Local_Kinetic(Walker)+potential(Walker)

def Observable_E(Walkers):
    """
    total ground state energy by average of local energies for walker
    """
    E=0.0
    Nw = len(Walkers)
    for i in range(Nw):
        E += E_local(Walkers[i])
    E /= Nw
    return E

def main():
    
    global a
    global Jastrow
    
    # wether to use jastrow or not
    Jastrow = False
    
    # tested | 1.1, 1.2, 1.3, 1.4, 1.5 and 1.3 is best fit
    a = 1.3
    
    # init random walker for vmc
    Walkers=[]
    Walkers.append(Walker(Ne=2,
                          Re=[array([0.5,0,0]),array([-0.5,0,0])],
                          spins=[0,1],
                          Nn=2,
                          Rn=[array([-0.75,0,0]),array([0.75,0,0])],
                          Zn=[1.0,1.0],
                          dim=3))
    
    vmc_only = True
    Ntarget=100
    vmc_time_step = 10.0
    Nblocks = 100
    
    Walkers, Eb_nj, Acc_nj = vmc_run(Nblocks,50,vmc_time_step,Walkers,Ntarget)
    
    E_mean_nj = mean(Eb_nj)
    E_std_nj = std(Eb_nj)/sqrt(Nblocks)
    

    x = linspace(1, Nblocks, Nblocks)
    
    # Plot results:
    fig = figure()
    ax = fig.add_subplot(111)
    
    ax.plot(x, Eb_nj, linestyle='None', marker='x', color='k',    \
            label='Block energies')
    ax.plot(array([1, Nblocks]), array([E_mean_nj, E_mean_nj]), linestyle='-', \
            color='b', label='Average energy')
    ax.plot(array([1, Nblocks]), array([E_mean_nj + E_std_nj, E_mean_nj +\
            E_std_nj]), linestyle='--', color='r', label='Standard deviation')
    ax.plot(array([1, Nblocks]), array([E_mean_nj - E_std_nj, E_mean_nj - \
            E_std_nj]), linestyle='--', color='r')
                   
    grid(True)
    xlabel('Blocks')
    ylabel('Energy')
    title('Problem 1 VMC energies without Jastrow')
    legend()
    
    # wether to use jastrow or not
    Jastrow = True
    
    Walkers, Eb, Acc = vmc_run(Nblocks,50,vmc_time_step,Walkers,Ntarget)
    
    E_mean = mean(Eb)
    E_std = std(Eb)/sqrt(Nblocks)
    

    x = linspace(1, Nblocks, Nblocks)
    
    # Plot results:
    fig = figure()
    ax = fig.add_subplot(111)
    
    ax.plot(x, Eb, linestyle='None', marker='x', color='k',    \
            label='Block energies')
    ax.plot(array([1, Nblocks]), array([E_mean, E_mean]), linestyle='-', \
            color='b', label='Average energy')
    ax.plot(array([1, Nblocks]), array([E_mean + E_std, E_mean + E_std]),  \
            linestyle='--', color='r', label='Standard deviation')
    ax.plot(array([1, Nblocks]), array([E_mean - E_std, E_mean - E_std]),  \
            linestyle='--', color='r')
                   
    grid(True)
    xlabel('Blocks')
    ylabel('Energy')
    title('Problem 1 VMC energies With Jastrow')
    legend()
    
    if True:#not vmc_only:
        Nblocks_dmc = 10
        Niters_dmc = 10
        
        Walkers, Eb_dmc, Accept_dmc = dmc_run(Nblocks_dmc,Niters_dmc,Walkers, \
                                      0.05,mean(Eb),Ntarget) 
        

        E_mean_dmc = mean(Eb_dmc)
        E_std_dmc = std(Eb_dmc)/sqrt(Nblocks)
        

        x = linspace(1, Nblocks_dmc, Nblocks_dmc)
        
        fig2 = figure()
        ax = fig2.add_subplot(111)
        
        ax.plot(x, Eb_dmc, linestyle='None', marker='x', color='k', \
                label='Block energies')
        ax.plot(array([1, Nblocks_dmc]), array([E_mean_dmc, E_mean_dmc]), \
                linestyle='-', color='b', label='Average energy')
        ax.plot(array([1, Nblocks_dmc]), array([E_mean_dmc + E_std_dmc, \
                E_mean_dmc + E_std_dmc]), linestyle='--', color='r', \
                label='Standard deviation')
        plot(array([1, Nblocks_dmc]), array([E_mean_dmc - E_std_dmc, \
             E_mean_dmc - E_std_dmc]), linestyle='--', color='r')
             
        grid(True)
        xlabel('Blocks')
        ylabel('Energy')
        title('Problem 1 DMC energies')
        legend()
    
    print(f'\nVMC No Jastrow\nAverage acceptance: {mean(Acc_nj)}')
    print(f'Average Energy: {E_mean_nj}')
    print(f'Standard deviation: {E_std_nj}')
    print(f'Variance to energy ratio: {abs(E_std_nj**2/E_mean_nj)}')
    
    print(f'\nVMC Jastrow\nAverage acceptance: {mean(Acc)}')
    print(f'Average Energy: {E_mean}')
    print(f'Standard deviation: {E_std}')
    print(f'Variance to energy ratio: {abs(E_std**2/E_mean)}')
    
    print(f'\nDMC\nAverage acceptance: {mean(Accept_dmc)}')
    print(f'Average Energy: {E_mean_dmc}')
    print(f'Standard deviation: {E_std_dmc}')
    print(f'Variance to energy ratio: {abs(E_std_dmc**2/E_mean_dmc)}')
    
    show()
    
if __name__=="__main__":
    main()
        
