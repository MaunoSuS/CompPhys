----------------- EXERCISESET 9 ------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: Calculated energy of hydrofen molecule using qmcpack with vmc 
          and dmc.

problem2: Initalizing qmcpack on PUHTI 

problem3: Diamond lattice energy with vmc. Calculation took too long for some
          reason. However scf gave an energy

problem4: Diamond lattice energy with vmc with and without jastrow and dmc. 
          Calculation took too long for some reason. However scf gave an energy

