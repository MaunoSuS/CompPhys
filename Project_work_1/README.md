------------ FYS-4096 COMPUTATIONAL PHYSICS - PROJECT WORK 1 -------------
------------- QUANTUM MECHANICAL MAXWELL'S DEMON SIMULATION --------------
                     

**************************************************************************
***                        Matias Mustonen 267207                      ***
**************************************************************************

This folder contains the files necessary for the
simulation of propagation of a wave packet in 
a two cavity system.

### warm_up.py       warm-up exercise
### obf_file.py      reads data from .obf files
### probability.py   calculates the probability distribution in the system 
### --------------   at every time interval and plots it against time
### write_inp.py     writes a modified version according to input parameters 
### ------------     of inp file to simulation folder
### convergence.py   convergence calculation for grid size simulation
### inp              simulation input file for octopus
### grid_size_sim    bash-script for checking grid sizes for the real simulation
### simulation       bash-script that creates simulation folders in where it 
### ----------       then simulates using octopus and calculates the probability 
### ----------       evolution of the simulation. 
### ----------       (using python files and inp mentioned)
### post_process     bash-script for calculating probability, auto-correlation
### ----------       locality and plotting
### grid_sim         grid size simulation for running on PUHTI
###                  so not really to be used here
### abs_dif.png      Absolute difference convergence figure
### max_dif.png      Average and maximum difference convergence figure
### Presentation.pdf PDF version of the presentation

------------------------------ IN FOLDERS -----------------------------
### AutoCorrelation  Autocorrelation figure
### {}.png
### auto_loc.txt     Points of the autocorerlation and locality figures
### averages.txt     Averages of the later half of the simulation
### density_anima    Animation of the simulation
### tion{}.mp4
### Figure{}.png     Probability plot
### LocalityIE{}.png Information entropy plot
### LocalityIPR{}/png Inverse participation ratio plot
### parameters.txt   Parameters of the simulation
### value.txt        Probability plot values
 
(grid_size_sim and simulation should be modified to suit simulation needs.
Instructions in said files. Inp file is automatically changed by write_inp.py
call in bash-scripts thus it is not needed to be changed.

---------------------!-!- DO NOT ALTER INP FILE -!-!------------------------

########################### ORDER OF COMPUTING ############################
                         for grid size simulation

- bash grid_size_sim
- bash post_process
- python3 convergence.py -- N {Number of simulation directories} -- GRID True


                          for actual simulation
- bash simulate
- bash post_process

########################### THEORY AND RESULTS ############################

- Choose a good grid size
- With changing momentum and starting location (box vs stadium) check how
  the system evolves
- Probability calculated tells the probabilities wheter the wavepacket is 
  in box or stadium
  - we see that when started in the box the probability is higher to be in
    box rather than when started in stadium the packet being in the stadium
  - If we set the system and measure after certain amount of time (when the 
    system has spread out) we can conclude from the results that most of
    the measurements would end up in the box
  - i.e we see the quantum mechanical Maxwell's demon in action
  - However work cannot be extracted from the pressure difference thus 
    2nd law of thermodynamics is not broken
- Autocorrelation tells how much the system resembles the initial state
  - we see that at no point it really resembles the initial state
  - except when starting in the box we see correlation up to 0.5 which is 
    pretty high. This corresponds to the forming of "blobs" in the animation
    as do the major dips in the autocorrelation since most of the wave is in 
    a blob but not near the original position.
- Locality tells how spread out the system is
  - we can infer that the wave-packet does spread out but we see from 
    simulation video and probability figure that while it does spread out to
    the whole system it does prefer the starting cavity
  - also we see that when starting in the box the locality figures show
    distinctive dips that correspond to the forming of "blobs" in the animation
