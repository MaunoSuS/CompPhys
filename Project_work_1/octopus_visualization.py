from obf_file import read_array

import argparse
import decimal
import glob
import re

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.tri as tri
from matplotlib.animation import FuncAnimation

# import progressbar


def read_mesh(dir_path):
    """
    Reads and returns the mesh points from
    an Octopus simulation.

    Input
    -----
    dir_path: Text
        Directory where the output of the simulation step is

    Output
    ------
    x: np.array
        x-coordinates of the mesh
    y: np.array
        y-coordinates of the mesh
    """
    x = read_array(f'{dir_path}/mesh_r-x.obf')
    y = read_array(f'{dir_path}/mesh_r-y.obf')
    return x, y


def read_density(directory_path):
    """Reads the OBF-format density saved by Octopus"""
    dens = read_array(f'{directory_path}/density.obf')
    return dens


def compute_triangulation(x, y):
    """Computes triangulation of points defined by x and y"""
    triang = tri.Triangulation(x, y)
    # Remove zero-area triangles per
    # http://matplotlib.1069221.n5.nabble.com/Matplotlib-with-invalid-triangulations-tp47627p47628.html
    xy = np.dstack((triang.x[triang.triangles], triang.y[triang.triangles]))
    twice_area = np.cross(xy[:, 1, :] - xy[:, 0, :], xy[:, 2, :] - xy[:, 0, :])
    mask = twice_area < 1e-10
    
    
    if np.any(mask):
        triang.set_mask(mask)

    return triang


def plot_frame(ax, z, X, Y, triang, im, vmax):
    """Draws/updates a single frame in the animation"""
    # Construct interpolator
    interpolator = tri.LinearTriInterpolator(triang, z)

    # Interpolate points
    Zi = interpolator(X, Y)
    
    # Previous imshow exists, just update the values
    if im:
        im.set_data(Zi.T)
        if not vmax:
            im.autoscale()
    else:
        im = ax.imshow(Zi.T,
                       origin='bottom',
                       extent=[X.min(), X.max(),
                               Y.min(), Y.max()],
                       vmin=0,
                       vmax=vmax,
                       interpolation='hanning')
        ax.set_xlim([X.min(), X.max()])
        ax.set_ylim([Y.min(), Y.max()])
        ax.set_aspect(1)
        ax.autoscale(False)
        
        ax.add_patch(patches.Rectangle((-1.5,0.0), 0.5, 2, color='w'))
        ax.add_patch(patches.Rectangle((1,0.0), 0.5, 2, color='w'))
        ax.add_patch(patches.Rectangle((-1.5,1.3926), 1.3, 0.6074, color='w'))
        ax.add_patch(patches.Rectangle((0.2,1.3926), 1.3, 0.6074, color='w'))
    
    return im,


def main():

    # Setup CLI
    parser = argparse.ArgumentParser(
        description='Animate Octopus\' time dependent simulation.')

    parser.add_argument('--basepath',
                        type=str,
                        help='Basepath to \'output_iter\'',
                        default='.',
                        required=False)

    parser.add_argument('--vmax',
                        type=float,
                        help='Maximum particle density in the colorbar.',
                        default=None,
                        required=False)
    parser.add_argument('--dt',
                        type=decimal.Decimal,
                        help='Time step of the simulation',
                        required=True)
    parser.add_argument(
        '--out',
        type=str,
        help=
        'Output file path. If provided, will not show interactive visualization, but rather saves the animation to the file provided.',
        default=None,
        required=False)
    parser.add_argument(
        '--resolution',
        type=int,
        help='Number of interpolation points in the x-direction.',
        default=200)

    # Parse the arguments
    args = parser.parse_args()
    vmax = args.vmax
    dt = args.dt

    # Read all output directories
    # and sort them
    frames = glob.glob(f'{args.basepath}/data/td.*')
    frames.sort(
        key=lambda x: int(re.match(r'.+/td\.(?P<iter>\d+)', x).group('iter')))

    # Read mesh from the first dir
    x, y = read_mesh(frames[0])
    xmax = x.max()
    ymax = y.max()

    # Generate the visualization mesh
    num_xpts = args.resolution
    num_ypts = int(np.ceil(ymax / (2 * xmax) * num_xpts))
    Xi, Yi = np.mgrid[-xmax:xmax:num_xpts * 1j, 0:ymax:num_ypts * 1j]
    triang = compute_triangulation(x, y)

    # Initialize the figure and axis
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel(r'$x$ (a.u.)')
    ax.set_ylabel(r'$y$ (a.u.)')
    # im shall hold the handle for the image of the density
    im = None

    # Progressbar
    # bar = progressbar.ProgressBar(max_value=len(frames)+1)

    # Read dennsity
    dens = read_density(frames[0])

    # Compute the iteration number and time
    framenum = int(
        re.match(r'.+/td\.(?P<iter>\d+)', frames[0]).group('iter'))
    time = framenum * dt

    # Plot the density
    im, = plot_frame(ax, dens, Xi, Yi, triang, im, vmax)

    # Draw colormap
    cbar = fig.colorbar(im, ax=ax)
    cbar.set_label(r'$\vert \psi(x,y,t) \vert^2$')

    # Set title
    fig.suptitle(f'$T={time}$')


    def func_update(frame, *args):
        im = args[0]
        # bar.update(bar.value + 1)

        # Compute the iteration number and time
        framenum = int(re.match(r'.+/td\.(?P<iter>\d+)', frame).group('iter'))
        time = framenum * dt

        dens = read_density(frame)

        # Update the density plot
        im, = plot_frame(ax, dens, Xi, Yi, triang, im, vmax)

        # Update time
        fig.suptitle(f'$T={time}$')
        return im

    # Animate
    anim = FuncAnimation(fig,
                         func_update,
                         frames=frames,
                         repeat=False,
                         fargs=(im,))

    if args.out:
        if not args.out.endswith('.mp4'):
            print("Output file should be of type '.mp4'")
            exit(1)
        anim.save(f'{args.out}')
    else:
        plt.show()


if __name__ == '__main__':
    main()
