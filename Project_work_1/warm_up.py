""" 
-------- PROJECT WORK 1 - warm up ------------
----- FYS-4096 - Computational Physics -----

Calculates 2d integral of (x+y)e^-0.5*sqrt(x^2+y^2) on an area defined by
a1 = (1.2, 0)  a2 = (0.6, 1) (should be close = 0.925713)

:function: calculate: calculates and plots the integral
"""

from scipy.integrate import simps
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def calculate():
    """
    Calculates the integral of (x+y)e^-0.5*sqrt(x^2+y^2) over
    the area defined by lattice vectors a1 = (1.2, 0)  a2 = (0.6, 1)
    """
    # function to integrate
    func = lambda x, y: (x + y) * np.exp(-0.5 * np.sqrt(x ** 2 + y ** 2))
    
    # lattice vectors
    a1 = (1.2, 0)
    a2 = (0.6, 1)

    # number of gridpoints
    N = 100
    
    # grid for lattice vectors
    A1, A2 = np.meshgrid(np.linspace(0, 1, N), np.linspace(0, 1, N))

    # cartesian version of the grid
    X = A1 * a1[0] + A2 * a2[0]
    Y = A1 * a1[1] + A2 * a2[1]
    
    # function values
    F = func(X, Y)
    
    # integrals for the row
    I = simps(F, X)
    
    # integrate over rows
    I = simps(I, Y[:, 0])
    print(f"Value of the integral is: {I}")

    # wireframe plot
    fig = plt.figure()
    ax = fig.add_subplot(121, projection='3d')
    ax.plot_wireframe(X, Y, F, rstride=10, cstride=10)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('F(x, y)')

    # make contour plot of integration area
    ax2 = fig.add_subplot(122)
    ax2.contourf(X, Y, F)
    ax2.axis('equal')
    ax2.set_xlabel('x')
    ax2.set_ylabel('y')

    plt.show()

def main():
   calculate()

if __name__ == "__main__":
    main()
