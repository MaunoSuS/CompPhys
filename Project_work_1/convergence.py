"""
Computational Physics FYS-4096 Project Work 1 Simulation of quantum mechanical
Maxwell's demon

Takes in the plotted data points from grid testing simulations and 
does convergence check with said points
"""


from os import fdopen, remove, path
import numpy as np
import matplotlib.pyplot as plt
import argparse

def readfile(num):
    """
    reads value.txt corresponding to num:th grid size simulation
    """
    
    try:
        f = open(f"Simulation/Sim_grid{num}/value.txt","r")
        
        b = np.zeros([1000,1])
        s = np.zeros([1000,1])
        i = 0
        for line in f:
            
            #print(line)
            info = line.split()
            b[i] = float(info[0])
            s[i] = float(info[1])
            i += 1
        
        if i < 1000:
            b = fill(b[0:i])
            s = fill(s[0:i])
        
        return b, s
        
    except FileNotFoundError:
        print(f"did not find Simulation/Sim_grid{num}/value.txt")
        return 0, 0
    
def fill(vector):
    """
    fill a vector that is shorter than 1000
    """
    
    length = vector.size
    limit = 1000/length
    
    new_vector = np.zeros([1000,1])
    
    j = 0
    k = 1
    for i in range(0,999):
        if k > limit:
            new_vector[i] = vector[j]
            j += 1
            # k = 1
            k = k - limit + 1
        else:
            new_vector[i] = vector[j]
            k += 1
    
    if new_vector[-1] == 0: new_vector[-1] = vector[-1]
    
    return new_vector
    
    
def main():

    # Setup CLI
    parser = argparse.ArgumentParser(
        description='Convergence parameters')

    parser.add_argument('--N',
                        type=int,
                        help='Number of folders to look at',
                        default=1,
                        required=True)

    parser.add_argument('--GRID',
                        type=bool,
                        help='Grid simulation',
                        default=False,
                        required=False)

    # Parse the arguments
    args = parser.parse_args()
    
    # how many grid_size simulations are done
    N = args.N
    
    # and here is a bunch of plotting
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    ax1.grid(True)
    ax2.grid(True)
    
    # ax1.set_title("Box")
    # ax2.set_title("Stadium")
    
    b_real, s_real = readfile(N)
    b_dif = np.zeros([N-1,1])
    s_dif = np.zeros([N-1,1])
    b_ave = np.zeros([N-1,1])
    s_ave = np.zeros([N-1,1])
    
    # absolute difference convergence
    num = 1
    while num < N:
        
        b, s = readfile(num)
        # print(f"{b[-1]} {s[-1]}")
        
        ax1.plot(abs(b_real - b), label=f"{num}", linewidth=0.7)
        
        ax2.plot(abs(s_real - s), label=f"{num}", linewidth=0.7)
        b_dif[num-1] = max(abs(b_real - b))
        s_dif[num-1] = max(abs(s_real - s))
        b_ave[num-1] = np.sum(abs(b_real - b))/b.size
        s_ave[num-1] = np.sum(abs(s_real - s))/s.size
        
        num += 1
    
    # maximum and average difference convergence
    fig2 = plt.figure()  
    
    ax3 = fig2.add_subplot(221)
    ax3.set_title("Maximum")
    ax3.plot(b_dif , 'rx', label='Box')
    ax3.plot(s_dif , 'bx', label='Stadium')
    ax3.grid(True)
    
    ax4 = fig2.add_subplot(222)
    ax4.set_title("Average")
    ax4.plot(b_ave , 'rx', label='Box')
    ax4.plot(s_ave , 'bx', label='Stadium')
    ax4.grid(True)
    
    ax5 = fig2.add_subplot(223)
    ax5.semilogy(b_dif , 'rx', label='Box')
    ax5.semilogy(s_dif , 'bx', label='Stadium')
    ax5.grid(True)
    
    ax6 = fig2.add_subplot(224)
    ax6.semilogy(b_ave , 'rx', label='Box')
    ax6.semilogy(s_ave , 'bx', label='Stadium')
    ax6.grid(True)
    
    ax1.label_outer()
    ax2.label_outer()
    ax4.yaxis.tick_right()
    ax6.yaxis.tick_right()
    
    ax1.legend(loc='best',
           ncol=1, borderaxespad=0.)
    ax2.legend(loc='best',
           ncol=1, borderaxespad=0.)
    ax3.legend(loc='best',
           ncol=1, borderaxespad=0.)
    ax4.legend(loc='best',
           ncol=1, borderaxespad=0.)
    ax5.legend(loc='best',
           ncol=1, borderaxespad=0.)
    ax6.legend(loc='best',
           ncol=1, borderaxespad=0.)
    
    fig.savefig("abs_dif",dpi=400)
    fig2.savefig("max_dif",dpi=400)
    
    
    # plt.show()

if __name__ == '__main__':
    main()
