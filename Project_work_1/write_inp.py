"""
Computational Physics FYS-4096 Project Work 1 Simulation of quantum
mechanical Maxwell's demon

Creates new inp file on the basis of an old one with parameters determined 
when function is called. Used in the Quantum Mechanical Maxwell's Demon-
Project.
"""



import argparse
import decimal
from tempfile import mkstemp
from shutil import move, copymode
from os import fdopen, remove, path

def main():
    
    # Create CLI parser
    parser = argparse.ArgumentParser(
        description='Generate input file for Octopus')

    # Add some arguments
    parser.add_argument('-CalcMode',
                        type=str,
                        help='Calculation Mode, either gs or td',
                        required=True)
    
    parser.add_argument('-Tmax',
                        type=decimal.Decimal,
                        help='maximum time',
                        required=True)
                        
    parser.add_argument('-dt',
                        type=decimal.Decimal,
                        help='time step',
                        required=True)
                        
    parser.add_argument('-s',
                        type=decimal.Decimal,
                        help='spacing',
                        required=True)
                        
    parser.add_argument('-w', '--width',
                        type=decimal.Decimal,
                        help='Width of the state',
                        required=True)

    parser.add_argument('-x',
                        type=decimal.Decimal,
                        help='x-coordinate of the center of the wavepacket',
                        required=True)

    parser.add_argument('-y',
                        type=decimal.Decimal,
                        help='y-coordinate of the center of the wavepacket',
                        required=True)

    parser.add_argument('-p',
                        type=decimal.Decimal,
                        help='Norm of the average of the momentum operator',
                        required=True)
    parser.add_argument('-t', '--theta',
                        type=decimal.Decimal,
                        help='Counter-clockwise angle of the average momentum wrt. positive x-axis in multiples of pi',
                        required=True)

    args = parser.parse_args()

    sig = args.width
    x0 = args.x
    y0 = args.y
    theta = args.theta
    p = args.p
    CalcMode = args.CalcMode
    dt = args.dt
    s = args.s
    Tmax = args.Tmax
    
    # Define the string for User Defined State
    template_str = f"%UserDefinedStates\n  1 | 1 | 1 | formula | \"finitegaussian((x-{x0})^2+(y-{y0})^2,{sig})*exp(-i*{p}*cos({theta}*pi)*x-i*{p}*sin({theta}*pi)*y)\" | normalize_yes\n%\n".replace(" ", "")
    
    #Create temp file
    fh, abs_path = mkstemp()
    
    # open old version of file and new version
    with fdopen(fh,'w') as new_file:
        with open('inp') as old_file:
            b_multil = 0
            for line in old_file:
                #write into the new file and modify necessary lines
                
                if line[0:18] == "CalculationMode = ":
                    # print("CalculationMode")
                    new_file.write(f'CalculationMode = {CalcMode} \n')
                elif line[0:10] == "Spacing = ":
                    # print("Spacing")
                    new_file.write(f"Spacing = {s}\n")
                elif line[0:5] == "dt = ":
                    # print("dt")
                    new_file.write(f"dt = {dt}\n")
                elif line[0:7] == "Tmax = ":
                    # print("Tmax")
                    new_file.write(f"Tmax = {Tmax}\n")
                elif line == "%UserDefinedStates\n":
                    # print("%UserDefinedStates")
                    new_file.write(template_str)
                    b_multil = 2
                elif b_multil != 0:
                    b_multil -= 1
                else:
                    new_file.write(line)
                
                
    #Copy the file permissions from the old file to the new file
    copymode('inp', abs_path)
    #Remove original file
    remove('inp')
    #Move new file
    move(abs_path, 'inp')
    
    
    # create a file with parameters in it
    if path.isfile('parameters.txt'): 
        remove('parameters.txt')
        print("removed parameters.txt")
    
    f_parameters = open("parameters.txt","w+")
    f_parameters.write(f"sigma = {sig} \n")
    f_parameters.write(f"x0    = {x0} \n")
    f_parameters.write(f"y0    = {y0} \n")
    f_parameters.write(f"theta = {theta} \n")
    f_parameters.write(f"p     = {p} \n")
    f_parameters.write(f"dt    = {dt} \n")
    f_parameters.write(f"s     = {s} \n")
    f_parameters.write(f"Tmax  = {Tmax} \n \n")
    f_parameters.write(f"E     = {0.5*float(p)**2} \n")
    f_parameters.close()

if __name__ == "__main__":
    main()
