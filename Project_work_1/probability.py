"""
Computational Physics FYS-4096 Project Work 1 Simulation of quantum
mechanical Maxwell's demon

Integrate density of particle in the system and calculate autocorrelation
and locality (IPR, IE)
"""
from obf_file import read_array

import argparse
import decimal
import glob
import re
import argparse
from os import fdopen, remove, path

import numpy as np

import matplotlib.pyplot as plt



def read_mesh(dir_path):
    """
    Reads and returns the mesh points from
    an Octopus simulation.

    Input
    -----
    dir_path: Text
        Directory where the output of the simulation step is

    Output
    ------
    x: np.array
        x-coordinates of the mesh
    y: np.array
        y-coordinates of the mesh
    """
    x = read_array(f'{dir_path}/mesh_r-x.obf')
    y = read_array(f'{dir_path}/mesh_r-y.obf')
    return x, y


def read_density(directory_path):
    """Reads the OBF-format density saved by Octopus"""
    
    return read_array(f'{directory_path}/density.obf')
    

def main():

    FigN = 1
    
    # Create CLI parser
    parser = argparse.ArgumentParser(
        description='Plot FigN:th simulation data')
    
    # Add some arguments
    parser.add_argument('-FigN',
                        type=int,
                        help='Number of the figure to be saved',
                        required=False)
                        
    args = parser.parse_args()

    FigN = args.FigN

    # Read all output directories
    # and sort them
    frames = glob.glob(f'data/td.*')
    frames.sort(
        key=lambda x: int(re.match(r'.+/td\.(?P<iter>\d+)', x).group('iter')))
    
    # initalize vectors
    prob_b = np.zeros((len(frames),1))
    prob_s = np.zeros((len(frames),1))
    prob_t = np.zeros((len(frames),1))
    prob_out = np.zeros((len(frames),1))
    IPR = np.zeros((len(frames),1))
    IE = np.zeros((len(frames),1))
    A = np.zeros((len(frames),1))
    
    
    k = 0

    for frame in frames:
        # Read mesh from the first dir
        x, y = read_mesh(frame)
        xmax = x.max()
        ymax = y.max()
        
        # Read density from the first dir
        dens = read_density(frame)
        
        # min max
        b = [[0,0],[0,0]]
        s = [[0,0],[0,0]]
        t = [[0,0],[0,0]]
        
        # prob sum
        p_b = 0
        p_s = 0
        p_t = 0
        p_out = 0
        
        # autokorrelaatio
        a = 0
        
        # inverse participation ratio
        ipr = 0
        
        # information entropy
        ie = 0
        
        # counter
        c_b = 0
        c_s = 0
        c_t = 0
        
        smallest = [[1,1],[1,1]]
        
        # if this is the first image save it
        if k == 0:
            x_last = np.zeros((x.size,1))
            y_last = np.zeros((y.size,1))
            dens_first = np.zeros((y.size,1))
        
        for i in range(0,x.size-1):
            
            # autocorrelation error check. Checks wether the grid is recorded
            # similarly each frame
            if not (k==0):
                if not (x_last[i] == x[i] and y_last[i] == y[i]): 
                    print(f"k, i: x_last[i], x[i], y_last[i], y[i]")
                    print(f"{k}, {i}: {x_last[i]}, {x[i]}, {y_last[i]}, {y[i]}")
                    print(f"Autokorrelation could be wrong, mesh has changed")
            
            else:
                dens_first[i] = dens[i]
                    
            x_last[i] = x[i]
            y_last[i] = y[i]
            
            # our y values shouldn't be negative 
            if True: #y[i] >= 0:
                
                # determines unit area
                if x[i] < smallest[0][1] and x[i] != smallest[0][0]:
                    if x[i] < smallest[0][0]:
                        smallest[0][1] = float(smallest[0][0])
                        smallest[0][0] = float(x[i])
                    else:
                        smallest[0][1] = float(x[i])
                
                if y[i] < smallest[1][1] and y[i] != smallest[1][0]:
                    if y[i] < smallest[1][0]:
                        smallest[1][1] = float(smallest[1][0])
                        smallest[1][0] = float(y[i])
                    else:
                        smallest[1][1] = float(y[i])
                
                # Add the probability density value in to the probability of
                # the cavity were it is. i.e. simple Riemann sum
                if y[i] >= 0 and y[i] <= 1.3926 and x[i] <= 1 and x[i] >= -1:
                    # box
                    if b[0][0] == 0 and b[0][0] == 0 and b[0][0] == 0 and b[0][0] == 0:
                        b = [[x[i],x[i]],[y[i],y[i]]]
                    
                    elif x[i] < b[0][0]:
                        b[0][0] = x[i]
                    elif x[i] > b[0][1]:
                        b[0][1] = x[i]
                    elif y[i] < b[1][0]:
                        b[1][0] = y[i]
                    elif y[i] > b[1][1]:
                        b[1][1] = y[i]
                        
                    p_b += dens[i]
                    c_b += 1
                    
                    
                    
                elif y[i] >= 2 and y[i] <= 3 and x[i] >= -1.5 and x[i] <= 1.5:
                    # stadium
                    if s[0][0] == 0 and s[0][0] == 0 and s[0][0] == 0 and s[0][0] == 0:
                        s = [[x[i],x[i]],[y[i],y[i]]]
                    
                    elif x[i] < s[0][0]:
                        s[0][0] = x[i]
                    elif x[i] > s[0][1]:
                        s[0][1] = x[i]
                    elif y[i] < s[1][0]:
                        s[1][0] = y[i]
                    elif y[i] > s[1][1]:
                        s[1][1] = y[i]
                        
                    p_s += dens[i]
                    c_s += 1
                    
                elif y[i] >= 1.3926 and y[i] <= 2 and x[i] <=0.2 and x[i] >= -0.2:
                    # tunnel
                    if t[0][0] == 0 and t[0][0] == 0 and t[0][0] == 0 and t[0][0] == 0:
                        t = [[x[i],x[i]],[y[i],y[i]]]
                    
                    elif x[i] < t[0][0]:
                        t[0][0] = x[i]
                    elif x[i] > t[0][1]:
                        t[0][1] = x[i]
                    elif y[i] < t[1][0]:
                        t[1][0] = y[i]
                    elif y[i] > t[1][1]:
                        t[1][1] = y[i]
                        
                    p_t += dens[i]
                    c_t += 1
                    
                else:
                    p_out += dens[i]
                    #print("x: {}, y: {}, dens: {}".format(x[i],y[i],dens[i]))
                
                ipr += dens[i]**2
                if dens[i] == 0:
                    ie = 0
                else:
                    ie += dens[i]*np.log(dens[i])
                    
                a += np.sqrt(dens[i]*dens_first[i])
        # end loop
        
        # unit area, uniform grid
        unit_area = (smallest[0][1]-smallest[0][0])*(smallest[1][1]-smallest[1][0])
        
        # probabilities at time t
        prob_b[k] = p_b *unit_area
        prob_s[k] = p_s *unit_area
        prob_t[k] = p_t *unit_area
        prob_out[k] = p_out *unit_area
        
        # autocorrelation at time t
        A[k] = a *unit_area
        
        # locality at time t
        if ipr*unit_area < 1e-6:
            IPR[k] = 1e6
        else:
            IPR[k] = 1/(ipr*unit_area)
        
        IE[k] = np.exp(-ie*unit_area)
        
        k += 1
    # end of for loop
    
    # we take 1000 terms uniformly from calculated vectors for plotting
    limit = (k/1000)
    riemann_b = 0.0
    riemann_s = 0.0
    
    if k == 0: return 0
        
    dif = 1.0/k
    if limit > 1:
    
        i = 1
        j = 0
        k = 0
        
        p_b = np.zeros([1000,1])
        p_s = np.zeros([1000,1])
        p_t = np.zeros([1000,1])
        p_o = np.zeros([1000,1])
        a = np.zeros([1000,1])
        ie = np.zeros([1000,1])
        ipr = np.zeros([1000,1])
        for thing in prob_b:
            if j > 500*limit:
                riemann_b += 2*dif*prob_b[j]
                riemann_s += 2*dif*prob_s[j]
            
            if i < limit: 
                i += 1
                j += 1
            else:
                
                if k == 0:
                    p_b[k] = prob_b[0]
                    p_s[k] = prob_s[0]
                    p_t[k] = prob_t[0]
                    p_o[k] = prob_out[0]
                    a[k] = A[0]
                    ie[k] = IE[0]
                    ipr[k] = IPR[0]
                else:
                    p_b[k] = prob_b[j]
                    p_s[k] = prob_s[j]
                    p_t[k] = prob_t[j]
                    p_o[k] = prob_out[j]
                    a[k] = A[j]
                    ie[k] = IE[j]
                    ipr[k] = IPR[j]
                
                j += 1
                k += 1
                i = i - limit + 1
        
        dt = np.linspace(0,1,1000)
    else:
        j = 0
        for thing in prob_b:
            if j > 0.5*k:
                riemann_b += 2*dif*prob_b[j]
                riemann_s += 2*dif*prob_s[j]
            j += 1
            
        p_b = prob_b
        p_s = prob_s
        p_t = prob_t
        p_o = prob_out
        dt = np.linspace(0,1,k)
        a = A
        ie = IE
        ipr = IPR
    
    print(f"integral box:{riemann_b} stadium:{riemann_s}")
    
    # plot probabilities
    fig_p = plt.figure()
    ax = fig_p.add_subplot(111)
    
    
    ax.plot(dt, p_b, 'b', label='Box')
    ax.plot(dt, p_s, 'g', label='Stadium')
    ax.plot(dt, p_t, 'r', label='Tunnel')
    ax.grid(True)
    
    if any(p_o > 0):
        ax.plot(dt, p_o, 'm', label='outside')
        ax.plot(dt, p_b+p_s+p_t+p_o, 'k', label='sum')
        print("probabilities outside!")
    
    ax.legend(loc='best',
           ncol=1, borderaxespad=0.)
    
    #  axis labels
    ax.set_xlabel('T')
    ax.set_ylabel('P')
    # ax.set_title(f"average box:{riemann_b} stadium:{riemann_s}")
    
    name = f"Figure{FigN}"
    # show the figure
    fig_p.savefig(name,dpi=200)
    #plt.show()
    
    # create a file with probability values
    if path.isfile('value.txt'): 
        remove('value.txt')
        print("removed value.txt")
    
    f_value = open("value.txt","w+")
    
    i = 0
    for value in p_b:
        if i == 1000: break
        f_value.write(f"{p_b[i][0]} {p_s[i][0]}\n")
        i += 1
    
    # create a file with autocorrelation and locality values
    if path.isfile('auto_loc.txt'): 
        remove('auto_loc.txt')
        print("removed auto_loc.txt")
    
    f_value = open("auto_loc.txt","w+")
    
    i = 0
    for value in a:
        if i == 1000: break
        f_value.write(f"{a[i][0]} {ie[i][0]} {ipr[i][0]}\n")
        i += 1
       
    # create a file with averages in it
    if path.isfile('averages.txt'): 
        remove('averages.txt')
        print("removed averages.txt")
    
    f_averages = open("averages.txt","w+")
    f_averages.write(f"box average, stadium average \n")
    f_averages.write(f"{riemann_b}, {riemann_s}")
    f_averages.close()
    
    # locality plot
    fig_ipr = plt.figure()
    ax_ipr = fig_ipr.add_subplot(111)
    fig_ie = plt.figure()
    ax_ie = fig_ie.add_subplot(111)
    
    ax_ipr.semilogy(dt, ipr)
    ax_ie.plot(dt, ie)
    
    ax_ipr.set_xlabel('T')
    ax_ipr.set_ylabel('IPR')
    ax_ie.set_xlabel('T')
    ax_ie.set_ylabel('IE')
    
    
    name_ipr = f"LocalityIPR{FigN}"
    
    fig_ipr.savefig(name_ipr,dpi=200)
    
    name_ie = f"LocalityIE{FigN}"
    
    fig_ie.savefig(name_ie,dpi=200)
    
    # autocorrelation plot
    fig_ac = plt.figure()
    ax_ac = fig_ac.add_subplot(111)
    
    ax_ac.plot(dt, a)
    ax_ac.set_xlabel('T')
    ax_ac.set_ylabel('AC')
    
    name_ac = f"AutoCorrelation{FigN}"
    
    fig_ac.savefig(name_ac,dpi=200)
    
if __name__ == '__main__':
    main()
