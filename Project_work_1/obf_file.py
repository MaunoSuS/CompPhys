import struct
import numpy as np

_octopus_array_type = {
    0: np.float32,
    1: np.float64,
    2: np.complex64,
    3: np.complex128,
    4: np.int32,
    5: np.int64
}


def parse_header(header_bytes):
    """
    Converts Octopus' output OBF-file's header
    to human readable format and detects endianness.

    Input
    -----
    header_bytes : bytes, len=64
        First 64 bytes from the file.

    Output
    ------
    endianness: str
        Either '<' for little endian or '>' for big endian
    array_size: int
        Size of the stored array
    array_dtype: int
        Type of the array
    """
    assert len(header_bytes) == 64
    assert isinstance(header_bytes, bytes)

    out = struct.unpack('>7cbIfQdQI5I', header_bytes)
    if out[8] == 1 and out[9] == 1 and out[10] == 1 and out[11] == 1:
        endianness = '>'
    else:
        endianness = '<'
        out = struct.unpack('<7cbIfQdQI5I', header_bytes)
        assert out[8] == 1 and out[9] == 1 and out[10] == 1 and out[
            11] == 1, "Could not detect endianness"

    array_size = out[12]
    array_dtype = np.dtype(_octopus_array_type[out[13]])
    array_dtype = array_dtype.newbyteorder(endianness)
    return array_size, array_dtype


def read_array(filepath):
    """
    Reads an array from an Octopus' OBF file.

    Input
    -----
    filepath: Text
        Path to the file

    Output
    ------
    np.array
      The array stored in the file as a correct datatype.
    """
    with open(filepath, 'rb') as f:
        content = f.read()

    size, dtype = parse_header(content[:64])

    return np.frombuffer(content[64:], dtype=dtype, count=size).copy()
