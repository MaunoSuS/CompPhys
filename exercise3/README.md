----------------- EXERCISESET 3------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: integrates given function with simpsons method
          and plots convergence of the numerical integral
          
problem2: interpolates given function along a given line 
          and compares it to a real solution and uninterpolated
          solution
          
problem3: solves the highest eigenvalue and corresponding eigenvector
          of a matrix with the power method and compares it to scipys
          own eigensolver
          
problem4: solves electric field around a one dimensional rod in two
          dimensional space, also creates a quiver plot of it around 
          the rod
