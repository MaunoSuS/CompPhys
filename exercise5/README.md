----------------- EXERCISESET 5------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: modified 4th order Runge-Kutta method in to file runge_kutta.py
          also prints maximum absolute diffrence of it with respect
          to scipys methods and also made comments
          
problem2: Program calculates the trajectory of a charged particle in 
          a electric and magnetic field and plots the path and 
          velocity 
          
problem3: Solves the Poisson partial differential equations with three
          differential and plots the wireframe of all of them and 
          tells the iteration loop amount
          
problem4: Solves the potential on two dimensions created by two capacitor plates
          plots the wireframe and solves the electric field caused by it and
          quiver plots it.
