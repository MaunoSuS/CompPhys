----------------- EXERCISESET 1------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: setting up system (Linux)
problem2: initalize git and create README's
problem3: program differentials (derivatives and
	  numerical integrations)
problem4: plotting convergence
problem5: publishing and packaging num_calculus
