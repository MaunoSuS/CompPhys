"""
FYS-4096 Computational Physics EX8. Problem 4b
Plots the convergence of diamond lattice energy with respect to 
lattice grid (k-grid) using quantum espresso
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

def main():
    
    # separations
    k = np.array([1, 
                  2, 
                  3])
    
    # values copied from run files
    E = np.array([-20.54419787,
                  -22.52203164,
                  -22.71796891])
    
    E_spline = interp1d(k,E, kind='linear')
    x = np.linspace(k[0],k[-1],100)
    
    fig = plt.figure()
    
    ax = fig.add_subplot(111)
    
    ax.plot(k, E, 'o')
    ax.plot(x, E_spline(x), linestyle="-", marker="")
    
    ax.set_xlabel("(k,k,k)")
    ax.set_ylabel("Total energy (Ry)")
    ax.set_title("Problem 4b: Diamond energy (k,k,k) convergence")
    plt.show()
    
if __name__=="__main__":
    main()
