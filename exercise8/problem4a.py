"""
FYS-4096 Computational Physics EX8. Problem 4a
Plots the convergence of diamond lattice energy with respect to 
cut-off energy (Ry) using quantum espresso
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

def main():
    
    # separations
    cut = np.array([100, 
                    150, 
                    200, 
                    250])
    
    # values copied from run files
    E = np.array([-22.51487991,
                  -22.52203164,
                  -22.52257905,
                  -22.52262424])
    
    E_spline = interp1d(cut,E, kind='linear')
    x = np.linspace(cut[0],cut[3],100)
    
    fig = plt.figure()
    
    ax = fig.add_subplot(111)
    
    ax.plot(cut, E, 'o')
    ax.plot(x, E_spline(x), linestyle="-", marker="")
    
    ax.set_xlabel("cut-off (Ry)")
    ax.set_ylabel("Total energy (Ry)")
    ax.set_title("Problem 4a: Diamond energy cut-off convergence")
    plt.show()
    
if __name__=="__main__":
    main()
