----------------- EXERCISESET 8 ------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: Solved 1D many interactiong electrons shrodinger equation with
          Hartree-Fock approximation

problem2: Initalizing PUHTI "profile"

problem3: Calculating potential energy surface of O2 near equilibrium
          distance using Quantum Espresso.

problem4: Also with Quantum Espresso
          the convergence of diamond lattice energy with respect to
          cut-off energy and lattice-grid (k-grid)
