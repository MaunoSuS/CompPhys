"""
FYS-4096 Computational Physics EX8. problem3
Plots the values of the O2 potential surface with respect to 
distance of the atoms in Angstrom (close to equilibrium) using
quantum espresso (plotted also polynomial and spline fitted curves)
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

def main():
    
    # separations
    d = np.array([1.0574, 
                  1.128, 
                  1.2074, 
                  1.288, 
                  1.352])
    
    # values copied from run files
    E = np.array([-63.60581888,
                  -63.67767344,
                  -63.70049255,
                  -63.68606183,
                  -63.65922700])
    
    E_poly = np.poly1d(np.polyfit(d,E,2))
    E_spline = interp1d(d,E, kind='cubic')
    x = np.linspace(d[0],d[4],100)
    
    fig = plt.figure()
    
    ax = fig.add_subplot(111)
    
    ax.plot(d, E, 'o')
    ax.plot(x, E_poly(x), linestyle="-", marker="", label="poly")
    ax.plot(x, E_spline(x), linestyle="-", marker="", label="spline")
    ax.legend()
    ax.set_xlabel("separation (Å)")
    ax.set_ylabel("Total energy (Ry)")
    ax.set_title("Problem 3: O$_2$ potential energy curve")
    plt.show()
    
if __name__=="__main__":
    main()
