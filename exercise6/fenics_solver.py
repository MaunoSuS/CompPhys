""" 
---------- EXERCISE 6 - problem 3 -----------
----- FYS-4096 - Computational Physics ------

Solves Poisson equation and heat equation with
fenics and shows solution

:function: boundary        return boundary for heat equation
:function: poisson         solves poisson equation with fenics
:function: heat_eq         solves heat equation with fenics    
:main:                     calls poisson and heat_eq  
"""



from fenics import *
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D


def boundary(x, on_boundary):
    return on_boundary

def poisson():
    """
    Solves and displays solution as shown in fenics tutorial:
    http://hplgit.github.io/fenics-tutorial/pub/sphinx1/._ftut1003.html
    d^2 u = f where f = -6
    """
    
    # Create mesh and define function space
    mesh = UnitSquareMesh(8, 8)
    V = FunctionSpace(mesh, 'P', 1)

    # Define boundary condition
    u_D = Expression('1 + x[0]*x[0] + 2*x[1]*x[1]', degree=2)

    bc = DirichletBC(V, u_D, boundary)

    # Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)
    f = Constant(-6.0)
    a = dot(grad(u), grad(v))*dx
    L = f*v*dx

    # Compute solution
    u = Function(V)
    solve(a == L, u, bc)

    # Plot solution
    u.rename('u', 'solution')
    plot(u)
    plot(mesh)

    # Save solution to file in VTK format
    vtkfile = File('poisson.pvd')
    vtkfile << u

    # Compute error in L2 norm
    error_L2 = errornorm(u_D, u, 'L2')

    # Compute maximum error at vertices
    vertex_values_u_D = u_D.compute_vertex_values(mesh)
    vertex_values_u = u.compute_vertex_values(mesh)
    import numpy as np
    error_max = np.max(np.abs(vertex_values_u_D - vertex_values_u))

    # Print errors
    print('error_L2  =', error_L2)
    print('error_max =', error_max)
    
    plot(u)
    plt.show()

def heat_eq():
    """
    Solves and animates the evolution of heat equation 
    as shown in fenics tutorial:
    https://fenicsproject.org/pub/tutorial/sphinx1/._ftut1004.html
    """
    T = 2.0  # final time
    num_steps = 50  # number of time steps
    dt = T / num_steps  # time step size

    # Create mesh and define function space
    nx = ny = 30
    mesh = RectangleMesh(Point(-2, -2), Point(2, 2), nx, ny)
    V = FunctionSpace(mesh, 'P', 1)

    bc = DirichletBC(V, Constant(0), boundary)

    # Define initial value
    # Donut shape initial value
    u_0 = Expression('exp(-pow(a*a-x[0]*x[0]-x[1]*x[1], 2))',
                     degree=2, a=1.5)
    u_n = interpolate(u_0, V)

    # Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)
    f = Constant(0)

    F = u * v * dx + dt * dot(grad(u), grad(v)) * dx - (u_n + dt * f) * v * dx
    a, L = lhs(F), rhs(F)

    # Create VTK file for saving solution
    vtkfile = File('heat_gaussian/solution.pvd')

    # Time-stepping
    u = Function(V)
    t = 0
    
    # animation of solution
    plt.ion()
    for n in range(num_steps-1):
        # Update current time
        t += dt

        # Compute solution
        solve(a == L, u, bc)

        # Save to file and plot solution
        vtkfile << (u, t)
        im = plot(u)
        plt.colorbar(im)
        im.set_clim(0, 0.4)
        plt.draw()
        plt.pause(0.0001)
        plt.clf()

        # Update previous solution
        u_n.assign(u)
       
    plt.close()

def main():
    poisson()
    heat_eq()

if __name__ == "__main__":
    main()
