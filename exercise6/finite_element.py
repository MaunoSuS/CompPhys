""" 
---------- EXERCISE 6 - problem 2 -----------
----- FYS-4096 - Computational Physics ------

Solves 1d Poisson equation with dinite element method
with analytical and numerical integrations and plots
results and differences

:function: num_derivative  Simple 1d numerical derivative
:function: fem             FEM with analytical integration
:function: fem_num         FEM with numerical integration
:function: u               basis functions
:function: fem_parser      parses the solution together      
:main:                     plots and intializes     
"""



import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simps

def num_derivative(function, x, dx ):
    """
    Simple 1d numerical derivative
    
    :param: function    function to be derivated
    :param: x           1d location of derivation
    :param: dx          "accuracy"
    
    :return:            derivative of function at x with "accuracy" dx
    """
    return (function(x+dx)-function(x-dx))/2/dx

def fem(rho, grid):
    """
    finite element analysis function with integral solved with analytical
    solution
    
    :param: rho     Rightside rho function of poisson equation. Not used here 
                    since we have an analytical solution to the integral already
    :param: grid    grid points of the calculation
    
    :return:        coefficient a_i of the approximation
    """
    
    # initalize matrix A vector b grid-length N and grid separation h
    N = grid.shape[0]
    A = np.zeros((N, N))
    b = np.zeros(N)
    h = grid[1]-grid[0]
    
    # calculate the values in matrix A[i,j]
    # and values of vector b[i]
    for i in range(1,N-1):
        for j in range(1, N-1):
        
            # Analytical solution to calculate the A-matrix
            if i == j:
                A[j, i] = 2/h
            elif abs(i-j) == 1:
                A[j, i] = -1/h
                
        
        # Analytical solution for integrals to calculate the b-vector
        b[i] = np.pi/h*(grid[i-1]+grid[i+1]-2*grid[i])*np.cos(np.pi*grid[i]) + \
               1/h*(2*np.sin(np.pi*grid[i])-np.sin(np.pi*grid[i-1]) - \
               np.sin(np.pi*grid[i+1]))
            
    # this makes the matrix invertable
    A[0, 0] = 1
    A[-1, -1] = 1
    
    # a = A^-1 b
    return np.linalg.inv(A) @ b, A

def fem_num(rho, grid):
    """
    finite element analysis function with integral solved with numerical
    solution
    
    :param: rho     Rightside rho function of poisson equation.
    :param: grid    grid points of the calculation
    
    :return:        coefficient a_i of the approximation
    """
    
    # initalize matrix A vector b grid-length N and grid separation h
    N = grid.shape[0]
    A = np.zeros((N, N))
    b = np.zeros(N)
    h = grid[1]-grid[0]
    
    # calculate the values in matrix A[i,j]
    # and values of vector b[i]
    for i in range(1,N-1):
        for j in range(1, N-1):
        
            # Calculate numerically the integral of  ui'(x)uj'(x)dx
            # with simpsons method
            
            # define functions around ith and jth nodes
            fi = lambda x: u(i, x, grid)
            fj = lambda x: u(j, x, grid)
            
            # calculate the functions over whole grid (with denser calculations)
            x = np.linspace(0, 1, 1000)
            y = np.zeros(1000)
            for k in range(len(x)):
                y[k] = num_derivative(fi, x[k], 0.0001) * \
                       num_derivative(fj, x[k], 0.0001)
            A[i, j] = simps(y, x)
            
        # Calculating integral with simpsons rule
        xx = np.linspace(0, 1, 1000)
        yy = np.zeros(1000)
        for j in range(len(xx)):
            # pi here for reasons mystical
            # it is on the analytical formula too
            yy[j] = np.pi*rho(xx[j])*u(i, xx[j], grid)
        b[i] = simps(yy, xx)
        
    # this makes the matrix invertable
    A[0, 0] = 1
    A[-1, -1] = 1
    
    # by definition ui(0) = ui(1) = 0
    b[0] = 0
    b[-1] = 0
    
    # a = A^-1 b
    return np.linalg.inv(A) @ b, A


def u(i, x, grid):
    """
    Outputs the linearly independent local functions u_i(x) as defined
    in the materials of FYS-4096. We use the "set of hat functions"
    defined around the ith node
    
    :param: i     index of the node
    :param: x     1d position
    :param: grid  grid of the calculation
    
    :return:      value of the local function at x
    """
    
    # return a defined value if x is in the neighbourhood of ith node
    # h = grid[i]-grid[i-1] = grid[i+1]-grid[i]
    
    # (x-x_(i-1))/h
    if grid[i-1] < x < grid[i]:
        return (x - grid[i-1])/(grid[i]-grid[i-1])
    
    # (x_(i+1)-x)/h
    if grid[i] < x < grid[i+1]:
        return (grid[i+1] - x)/(grid[i+1]-grid[i])
        
    # otherwise
    return 0

def fem_parser(x, a, grid):
    """
    Solves the estimation of the function with calculated information
    i.e. parses the answer together
    
    :param: x        1d location
    :param: a        coefficients a gor each node i
    :param: grid     1d grid of calculation
    
    :return:         estimation of the function at x
    """
    y = 0
    h = grid[1]-grid[0]
    for i in range(1, len(grid)-1):
        y += u(i, x, grid)*a[i]
    return y


def main():
    
    # grid size and grid initalization
    N = 20
    grid = np.linspace(0, 1, N)
    
    # rho function of poisson eq
    rho = lambda x: np.pi*np.sin(np.pi*x)
    
    # coefficients for the analytically integrated fem
    a1, A1 = fem(rho, grid)
    # coefficients for the numerically integrated fem
    a2, A2 = fem_num(rho, grid)
    
    print("maximum absolute difference between analytically")
    print("integrated fem and numerically integrated fem is")
    print(abs(A2-A1).max())
    print("------------------------------------------------")
    print("average absolute difference between analytically")
    print("integrated fem and numerically integrated fem is")
    print(abs(A2-A1).mean())
    
    # how dense we draw the solutions
    # and initalize solution vectors
    x = np.linspace(0, 1, 50)
    y1 = np.zeros(50)
    y2 = np.zeros(50)
    for i in range(len(x)):
        y1[i] = fem_parser(x[i], a1, grid)
        y2[i] = fem_parser(x[i], a2, grid)
    
    # and then plot both fem and analytical solution
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.plot(x, y1, label='FEM, analytical int')
    ax1.plot(x, y2, label='FEM, numerical int')
    ax1.plot(x, np.sin(np.pi*x), label='Analytical solution')
    ax1.set_title("solutions")
    ax1.legend()
    
    
    ax2 = fig.add_subplot(212)
    ax2.plot(x, abs(y1-np.sin(np.pi*x)), label='FEM, analytical int')
    ax2.plot(x, abs(y2-np.sin(np.pi*x)), label='FEM, numerical int')
    ax2.set_title("abs dif")
    ax2.legend()
    
    plt.show()

if __name__ == "__main__":
    main()
