----------------- EXERCISESET 6------------------
         FYS-4096 Computational Physics

************************************************
**           Matias Mustonen 267207           **
************************************************


problem1: Solved by hand the weak form of Poisson equations of sorts
          
problem2: Solved 1d Poisson equation with analytical and numerical integrals
          and compared to analytical solution.
          
problem3: Solved Poisson equation and heat equation with fenics
          
problem4: Review of first half of the course
